# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 08:49:16 2019

@author: Rochlitz.R
"""


import numpy as np
from custEM.core import MOD
import matplotlib.pyplot as plt


# %% specify polynomial order, model, mesh and create interpolation mesh

p = 1
mod = 'p' + str(p)
mesh = 'e1_layered_double_tx'

rx_origs = np.array([[0., 0., -0.1],
                     [150., 100., -0.1]])

# create interpolation mesh for Rx positions
M = MOD(mod, mesh, 'E_RA', p=p, overwrite=True, serial_ordering=True, # mumps_debug=True,
        m_dir='./meshes', r_dir='./results')
M.IB.create_path_mesh(rx_origs, 'e1_rec')


# %% run computations, select approaches to use for calculation

approaches = [#'E_RA',  # rational arnoldi
              #'E_IE',  # implicit euler
              'E_FT',  # ft based
              ]

for approach in approaches:

    # initialize model amd parameters
    M = MOD(mod, mesh, approach, p=p, debug_level=10, serial_ordering=True,
            overwrite_mesh=True, overwrite_results=True,
            m_dir='./meshes', r_dir='./results')

    M.MP.update_model_parameters(sigma_ground=[1e-2, 2e-3],
                                 currents=[1., -1., 1., -1.])

    M.FE.build_var_form(log_t_min=-6,
                        log_t_max=-2,
                        n_log_steps=5,
                        )

    # solve FE problem and interpolate results
    M.solve_main_problem(interp_mesh='e1_rec_path')  # relevant for FT approach

    if 'FT' not in approach:  # IE and RA approach interpolation
        M.IB.interpolate('e1_rec_path')
    M.PP.merge_td_results('e1_rec_path')

