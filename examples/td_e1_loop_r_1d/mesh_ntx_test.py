# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen import meshgen_utils as mu


# define parameteres and two Rx positions
dim = 2e3
name = 'e1_layered_double_tx'
mesh_dir = './meshes'

rx_origs = [[0., 0., 0.], [150., 100., 0.]]
rx = mu.refine_rx(rx_origs, 1.)

# initialize world
M = BlankWorld(name=name,
               m_dir=mesh_dir,
               preserve_edges=True,
               x_dim=[-dim, dim],
               y_dim=[-dim, dim],
               z_dim=[-dim, dim],
               )

# build surface mesh
M.build_surface(#insert_loop_tx=[mu.loop_r(start=[-5., -5.], stop=[5., 5.],
                #                          n_segs=12),
                #                mu.loop_r(start=[-20., -20.], stop=[20., 20.],
                #                          n_segs=44)],
                #insert_line_tx=[mu.line_y(-20., 20., x=-50.,
                #                          n_segs=13)],
                insert_paths=rx)

# expand 2D surface mesh to 3D layered-earth mesh
M.build_layered_earth_mesh(2, [-200.])     # 100,   10,    500 Ohm-m
M.add_tx(mu.loop_c([0., 0., 0.], 1., n_segs=8, z=10.), False)
M.add_tx(mu.loop_c([0., 0., 0.], 1., n_segs=8, z=10.), False)
M.add_tx(mu.loop_c([-2., 0., 0.], 1., n_segs=8, z=10.), False)
M.add_tx(mu.loop_c([-2., 0., 0.], 1., n_segs=8, z=10.), False)

# append boundary mesh to increase overall domain size
M.extend_world(10., 10., 10.)

# export files and run TetGen
M.call_tetgen(tet_param='-pq1.2aA', export_vtk=True)
