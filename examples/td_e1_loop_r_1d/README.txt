﻿################################################################################
# # # # #                    time-domain Example 1                     # # # # #
################################################################################

      validation of all time-domain approaches, p1/p2, loop source model

################################################################################

    - Start with generating the mesh by running the *mesh_td_example_1.py* file:

        > python mesh_td_example_1.py
    
    - The *run_example_1.py* script can be called either in serial (also use
      mpirun syntax) or in parallel.

        > mpirun -n 1 python -u run_example_1.py
        > mpirun -n 4 python -u run_example_1.py  (recommended for p1)
        > mpirun -n 12 python -u run_example_1.py (and export OMP_NUM_THERADS=2,
                                                   recommended for p2)
    
    - Please note that the computation with calling the *run_td_example_1.py*
      script requires ~5 GB RAM, if 4 mpi-processes are used.  
    
    - The *plot_td_example_1_***.py* scripts will generate identical plots as
      shown in the corresponding publication (Rochlitz et al., 2021) in
      the *plots* directory.

        > python plot_td_example_1_dB.py (or *E*/*B_off*/*B_on* instead of *dB*)

################################################################################