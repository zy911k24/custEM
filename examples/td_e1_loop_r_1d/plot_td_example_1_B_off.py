# -*- coding: utf-8 -*-
"""
Created on Tue Dec 09 16:06:00 2014

@author: moouuzi
"""


import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams as rc
from scipy.integrate import cumtrapz
from scipy.interpolate import interp1d
import empymod


def resample(t_in, field_in, t_out):

    func = interp1d(t_in, field_in, kind='cubic')
    return(func(t_out))


prop_cycle = plt.rcParams['axes.prop_cycle']
clr = prop_cycle.by_key()['color']

# rc['axes.edgecolor'] = '#002C72'
# rc['xtick.color'] = '#002C72'
# rc['ytick.color'] = '#002C72'
# rc['axes.labelcolor'] = '#002C72'
# rc['text.color'] = '#002C72'

# Model
depth = [0, 80., 130.]      # m
res = [2e14, 100, 10, 500]  # Ohm-m

# Set el. perm. for air to zero
# (diff approx; improves early times,
# see https://empymod.readthedocs.io/en/stable/speedmemoryaccuracy.html#time-domain-land-csem)
eperm = [0, 1, 1, 1]

cmp = 2
stn = 0

fem_times = np.logspace(-6, -2, 41)

# Survey parameters
if stn == 1:
    rec = [150, 100, 0.001]  # A little bit inside for the e-fields
elif stn == 0:
    rec = [0, 0, 0.001]  # A little bit inside for the e-fields

if cmp == 0:
    rec_empymod = [*rec, 0, 0]
if cmp == 1:
    rec_empymod = [*rec, 90, 0]
if cmp == 2:
    rec_empymod = [*rec, 0, 90]

model = {
    'depth': depth,
    'res': res,
    'freqtime': fem_times,
    'htarg': {'pts_per_dec': -1},  # lagged convolution type DLF
    'epermH': eperm,
    'epermV': eperm,
    'strength': 1.,
    'verb': 1,
}

inp_dip = {
    # [x, y, z, azimuth, dip]
    # for z, set dip to 90
    # for y, set azimuth to 90
    'rec': rec_empymod,
    # for E, set to False; for H, set to True (for B, multiply by mu0)
    'mrec': True,
    'srcpts': 5  # Gaussian quadr. with 5 pts to simulate a finite length dip.
}

ref = +empymod.bipole(src=[+50, +50, -50, +50, 0.01, 0.01],
                      signal=-1, **inp_dip, **model)
ref += empymod.bipole(src=[+50, -50, +50, +50, 0.01, 0.01],
                      signal=-1, **inp_dip, **model)
ref += empymod.bipole(src=[-50, -50, +50, -50, 0.01, 0.01],
                      signal=-1, **inp_dip, **model)
ref += empymod.bipole(src=[-50, +50, -50, -50, 0.01, 0.01],
                      signal=-1, **inp_dip, **model)
ref = np.abs(ref) * 4. * np.pi * 1e-7 * 1e9

# %% plot B

path0 = './results/E_IE/e1_layered/'
path1 = './results/E_FT/e1_layered/'
path2 = './results/E_RA/e1_layered/'

fig = plt.figure(figsize=(8, 6))
gs = plt.GridSpec(2, 1)
ax0 = fig.add_subplot(gs[0])
plt.grid()
if stn == 0:
    plt.title('central loop Rx position')
elif stn == 1:
    plt.title('out-of-loop Rx position')
ax1 = fig.add_subplot(gs[1])
plt.grid()

symb = ['^:', 's-.']
cmp_str = ['x', 'y', 'z']
fs = 12

for j, p in enumerate(['1']):

    name0 = 'p' + p + '_j_off'
    name1 = 'p' + p + ''
    name2 = 'p' + p + '_j'

    B_field0, B_field1, B_field2 = [], [], []

    B_field0 = np.load(path1 + name1 + '_interpolated/H_shut_on_' +
                       'e1_rec_path.npy')[stn, cmp, :]
    y0 = np.abs(B_field0) * (4. * np.pi * 1e-7) * 1e9

    B_field1 = np.load(path1 + name1 + '_interpolated/H_shut_off_' +
                       'e1_rec_path.npy')[stn, cmp, :]
    y1 = np.abs(B_field1) * (4. * np.pi * 1e-7) * 1e9

    B_field2 = np.load(path1 + name1 + '_interpolated/H_shut_off_' +
                       'e1_rec_path.npy')[stn, cmp, :]
    B_field2 *= (4. * np.pi * 1e-7) * 1e9
    #fem_times2 = np.loadtxt(path2 + name2 + '_times.txt')
    fem_times2 = fem_times

    # shut_off
    y2 = np.abs(cumtrapz(B_field2[::-1], fem_times2[::-1],
                          initial=B_field2[-1]))[::-1]
    # shut on
    #y2 = np.abs(cumtrapz(B_field2, fem_times2, initial=B_field2[0]))
    y22 = resample(fem_times2, y2, fem_times)

    prms0 = (y0 - ref)/ref * 100.
    prms1 = (y1 - ref)/ref * 100.
    prms2 = (y22 - ref)/ref * 100.
    prms2[np.abs(prms2) < 0.02] *= 10.

# %% plotting part
    if j == 0:
        ax0.plot(fem_times, ref * 1e9, 'k', label='empymod')
    ax0.plot(fem_times, y0 * 1e9, symb[j], color=clr[0],
             markersize=3, label='IE p' + p)
    ax0.plot(fem_times, y1 * 1e9, symb[j], color=clr[1],
              markersize=3, label='FT p' + p)
    ax0.plot(fem_times, y22 * 1e9, symb[j], color=clr[2],
              markersize=1, label='RA p' + p)

    ax0.set_yscale('log')
    ax0.set_xscale('log')
    ax0.set_xlim(1e-6, 1e-2)
    ax0.set_xticklabels([])
    ax0.set_ylabel(r'$\mathbf{b_' + cmp_str[cmp] + '}$ (nT)')

    ax1.plot(fem_times[:], np.abs(prms0), symb[j], color=clr[0],
             markersize=3, label='IE p' + p)
    ax1.plot(fem_times[:], np.abs(prms1), symb[j], color=clr[1],
              markersize=3, label='FT p' + p)
    ax1.plot(fem_times[:], np.abs(prms2), symb[j], color=clr[2],
              markersize=1, label='RA p' + p)

    ax1.set_ylim(0.01, 100.)
    ax1.set_xlim(1e-6, 1e-2)
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    ax1.set_xlabel('t (s)')
    ax1.set_ylabel(r'$\epsilon_\mathbf{b_' + cmp_str[cmp] + '}$ (%)')

    ax0.legend()

plt.savefig('./plots/b' + cmp_str[cmp] + '_off_stn_' + str(stn) + '.pdf',
            bbox_inches='tight', pad_inches=0)
plt.show()
