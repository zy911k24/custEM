# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen import meshgen_utils as mu


# define parameteres and two Rx positions
dim = 2e3
name = 'e1_layered'
mesh_dir = './meshes'

rx = [[0., 0., -0.1], [150., 100., -0.1]]
rx_tri = mu.refine_rx(rx, 1., n_segs=3)

# initialize world
M = BlankWorld(name=name,
               m_dir=mesh_dir,
               preserve_edges=True,
               x_dim=[-dim, dim],
               y_dim=[-dim, dim],
               z_dim=[-dim, dim],
               )

# build surface mesh
M.build_surface(insert_loop_tx=[mu.loop_r(start=[-50., -50.], stop=[50., 50.],
                                          n_segs=200)],
                insert_paths=rx_tri)

# expand 2D surface mesh to 3D layered-earth mesh
M.build_layered_earth_mesh(3, [-80., -130.])     # 100,   10,    500 Ohm-m

# append boundary mesh to increase overall domain size
M.extend_world(10., 10., 10.)

# export files and run TetGen
M.call_tetgen(tet_param='-pq1.6aA', export_vtk=True)
