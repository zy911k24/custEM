# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 08:49:16 2019

@author: Rochlitz.R
"""


import numpy as np
from custEM.core import MOD

# %% specify polynomial order, model, mesh and create interpolation mesh

p = 2
mod = 'p' + str(p)
mesh = 'e2_lotem'

rx_origs = np.zeros((18, 3))
rx_origs[:6, 0] = -1e3
rx_origs[12:18, 0] = 1e3
rx_origs[:, 1] = np.tile(np.linspace(500., 3000., 6), 3)

# %% run computations, select approaches to use for calculation

approaches = ['E_IE',  # implicit euler
              'E_FT',  # fourer-transform based
              'E_RA',  # rational arnoldi
              ]

for approach in approaches:

    # initialize model amd parameters
    M = MOD(mod, mesh, approach, p=p, overwrite_results=True,
            overwrite_mesh=True, serial_ordering=True,
            m_dir='./meshes', r_dir='./results')

    M.MP.update_model_parameters(sigma_ground=[1e-2, 2e-2, 2e-3, 1e-1])
    M.IB.create_path_mesh(rx_origs, path_name='e2_rec', z=-1.)

    M.FE.build_var_form(n_lin_steps=60,  # only relevant for IE approach
                        n_log_steps=41,
                        source_type='djdt',  # only relevant for IE approach
                        log_t_min=-4,
                        log_t_max=-0,
                        interp_mesh='e2_rec_path',  # only relevant for FT app.
                        )

    # solve FE problem and interpolate results
    M.solve_main_problem(export_pvd=False)

    if 'FT' not in approach:  # IE and RA approach interpolation
        M.IB.interpolate('e2_rec_path')
    M.PP.merge_td_results('e2_rec_path')
