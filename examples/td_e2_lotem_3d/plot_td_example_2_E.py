# -*- coding: utf-8 -*-
"""
Created on Tue Dec 09 16:06:00 2014

@author: moouuzi
"""


import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams as rc
from scipy.integrate import cumtrapz
from matplotlib.colors import LogNorm
from scipy.interpolate import interp1d


def resample(t_in, field_in, t_out):

    func = interp1d(t_in, field_in, kind='cubic')
    return(func(t_out))


prop_cycle = plt.rcParams['axes.prop_cycle']
clr = prop_cycle.by_key()['color']


# %% plot B

path0 = 'results/E_IE/e2_lotem/'
path1 = 'results/E_FT/e2_lotem/'
path2 = 'results/E_RA/e2_lotem/'


symb = ['^:', 's-.']
fs = 12
cstr = ['x', 'y', 'z']

#cmp = 2
stn = 11
p = '2'
name = 'p' + p + ''

# rc['axes.edgecolor'] = '#002C72'
# rc['xtick.color'] = '#002C72'
# rc['ytick.color'] = '#002C72'
# rc['axes.labelcolor'] = '#002C72'
# rc['text.color'] = '#002C72'

pos = ['x-1000_y500',
       'x-1000_y1000',
       'x-1000_y1500',
       'x-1000_y2000',
       'x-1000_y2500',
       'x-1000_y3000',
       'x0_y500',
       'x0_y1000',
       'x0_y1500',
       'x0_y2000',
       'x0_y2500',
       'x0_y3000',
       'x+1000_y500',
       'x+1000_y1000',
       'x+1000_y1500',
       'x+1000_y2000',
       'x+1000_y2500',
       'x+1000_y3000']


fem_times = np.logspace(-4, 0, 41)

for kk, stn in enumerate(range(18)):
    fig = plt.figure(figsize=(8, 6))
    gs = plt.GridSpec(2, 1)
    E_field0, E_field1, E_field2 = [], [], []

    # E_field0 = np.load(path0 + name + '_interpolated/dH_shut_off_' +
    #                    name + '_on_e2_rec_path.npy')[stn, cmp, :]
    # y0 = np.abs(E_field0) * (4.* np.pi * 1e-7) * 1e9

    # E_field1 = np.load(path1 + name + '_interpolated/dH_shut_off_' +
    #                    name + '_on_e2_rec_path.npy')[stn, cmp, :]
    # y1 = np.abs(E_field1) * (4.* np.pi * 1e-7) * 1e9

    # E_field2 = np.load(path2 + name + '_interpolated/dH_shut_off_' +
    #                    name + '_on_e2_rec_path.npy')[stn, cmp, :]
    # y2 = np.abs(E_field2) * (4.* np.pi * 1e-7) * 1e9

    E_field0 = np.load(path0 + name + '_interpolated/E_shut_off_' +
                       'e2_rec_path.npy')[stn, :3, :].T
    y0 = np.linalg.norm(E_field0, axis=1)

    E_field1 = np.load(path1 + name + '_interpolated/E_shut_off_' +
                       'e2_rec_path.npy')[stn, :3, :].T
    y1 = np.linalg.norm(E_field1, axis=1)

    E_field2 = np.load(path2 + name + '_interpolated/E_shut_off_' +
                       'e2_rec_path.npy')[stn, :3, :].T
    y2 = np.linalg.norm(E_field2, axis=1)

    sldmem = np.abs(np.loadtxt('sldmem/e_' + pos[kk] + '_sldmem.logfile', skiprows=1)[:, 1:])
    ref = np.linalg.norm(sldmem, axis=1) * 1e-6


    prms0 = np.abs((y0 - ref)/(ref + y0)) * 200.
    prms1 = np.abs((y1 - ref)/(ref + y1)) * 200.
    prms2 = np.abs((y2 - ref)/(ref + y2)) * 200.

    # %% plotting part


    ax0 = fig.add_subplot(gs[0])
    plt.grid()
    ax4 = fig.add_subplot(gs[1])

    # for j in range(18):
    #     ref = sldmem[:, j]
    ax0.plot(fem_times, ref, 'k', label='SLDMEM')
    ax0.plot(fem_times, y0, symb[1], color=clr[0],
              markersize=3, label='IE p' + p)
    ax0.plot(fem_times, y1, symb[1], color=clr[1],
              markersize=2, label='FT p' + p)
    ax0.plot(fem_times, y2, symb[1], color=clr[2],
              markersize=1, label='RA p' + p)
    ax0.set_yscale('log')
    ax0.set_xscale('log')
    ax0.set_xlim(1e-4, 1e0)
    ax0.set_xlabel('')
    ax0.set_xticklabels([])
    ax0.set_ylabel(r'||$\mathbf{e}$|| (V/m)')
    ax0.set_title(r'||$\mathbf{e}$||')

    ax4.plot(fem_times[:], np.abs(prms0), symb[1], color=clr[0],
            markersize=3, label='IE p' + p)
    ax4.plot(fem_times[:], np.abs(prms1), symb[1], color=clr[1],
              markersize=2, label='FT p' + p)
    ax4.plot(fem_times[:], np.abs(prms2), symb[1], color=clr[2],
            markersize=1, label='RA p' + p)

    ax4.set_ylim(0.01, 100.)
    ax4.set_xlim(1e-4, 1e0)
    ax4.set_xscale('log')
    ax4.set_yscale('log')
    ax4.set_xlabel('t (s)')
    ax4.set_ylabel(r'$\epsilon_\mathbf{||e||}$ (%)')
#
    ax0.legend()
    plt.grid()

    plt.savefig('./plots/E_' + pos[kk] + '.pdf', bbox_inches='tight', pad_inches=0)
    # plt.show()
