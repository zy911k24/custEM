# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen import meshgen_utils as mu
import numpy as np

# specify general paraeters
dim = 1e6
brick_cell_size = 1e5
name = 'e2_lotem'
mesh_dir = './meshes'

# define Rx
rx_origs = np.zeros((18, 3))
rx_origs[:6, 1] = np.linspace(500., 3000., 6)
rx_origs[6:12, 1] = np.linspace(500., 3000., 6)
rx_origs[12:, 1] = np.linspace(500., 3000., 6)
rx_origs[6:12, 0] = 1000.
rx_origs[12:, 0] = -1000.
rx_origs[:, 2] = -1.
rx = mu.refine_rx(rx_origs, 2., 30.)

# initialize world
M = BlankWorld(name=name,
                m_dir=mesh_dir,
                preserve_edges=True,
                x_dim=[-dim, dim],
                y_dim=[-dim, dim],
                z_dim=[-dim, dim],
                )

# build surface mesh with Tx & Rx
M.build_surface(insert_line_tx=[mu.line_x(-500., 500., n_segs=101)],
                insert_paths=rx)

# expand 2D surface mesh to 3D halfspace mesh
M.build_halfspace_mesh()

# add brick anomaly 1, seperated into 2 parts to avoid intersections
M.add_brick([100., 500., -600.],
            [700., 1000., -200.],
            cell_size=brick_cell_size)

# add brick anomaly 2, seperated into 2 parts to avoid intersections
M.add_brick([-900., 1000., -600.],
            [100., 1400., -200.],
            cell_size=brick_cell_size)
M.marker_counter -= 1
M.add_brick([100., 1000., -600.],
            [700., 1400., -200.],
            cell_size=brick_cell_size)

# add brick anomaly 3, seperated into 2 parts to avoid intersections
M.add_brick([-900., 1000., -600.],
            [100., 1400., -800.],
            cell_size=brick_cell_size)
M.marker_counter -= 1
M.add_brick([-900., 1400., -600.],
            [100., 2000., -800.],
            cell_size=brick_cell_size)

# include three observation lines with six stations each
M.add_rx(rx_origs[:6])
M.add_rx(rx_origs[6:12])
M.add_rx(rx_origs[12:])

# avoid face intersections of connected bricks
M.remove_duplicate_poly_faces()

# export files and run TetGen
M.call_tetgen(tet_param='-pq1.6aA', export_vtk=True)
