# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 2                          # # # # #
# ########################################################################### #
# # # # #                    visualization script 2                   # # # # #
# ########################################################################### #

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
from custEM.post import PlotFD as Plot
from custEM.post import make_plain_subfigure_box

# define matplotlib parameters
rcParams['axes.unicode_minus'] = False
rcParams['figure.figsize'] = 8, 13

EH_flg = 'H'
fs = 10
Elim = [1e-10, 1e-4]
Hlim = [1e-6, 1e-2]
errlim = [1e-1, 1e1]
strd = 1
comps = ['x', 'y', 'z', 'mag']
slicE = 'surface_slice_z'
meshes = ['example_2_mesh_coarse',
          'example_2_mesh_fine']
mod = 'p1'
freqs_str = ['1', '10', '100', '1000']
fig, ax = make_plain_subfigure_box(6, 4, fs=fs, log_scale=False)

# inititalize figure and Plot class
P = Plot(mod=mod, mesh=meshes[0], approach='E_t',
         r_dir='./results', s_dir='./plots')

for j, freq in enumerate(freqs_str):
    P.rel_comp_errors = dict()
    data = np.load('./ref_data/E_pyhed_freq_' + freq + '.npy')
    data_s = P.reduce_slice_data(data, 3, 4, strd)
    P.slice_data['pyhed_ref_' + freq + '_E_t'] = data_s[:, :3]
    data = np.load('./ref_data/H_pyhed_freq_' + freq + '.npy')
    data_s = P.reduce_slice_data(data, 3, 4, strd)
    P.slice_data['pyhed_ref_' + freq + '_H_t'] = data_s[:, :3]

    for jj, mesh in enumerate(meshes):
        mod1 = 'p1'
        t1 = 'Tp1' + freq + str(jj)
        P.import_slice_data(slicE, mod=mod1, mesh=mesh, key=t1,
                            stride=strd, freq=j)
        for flg in ['_E_t', '_H_t']:
            P.rel_errors(P.slice_data[t1 + flg],
                         P.slice_data['pyhed_ref_' + freq + flg])

    t2 = 'Tp2' + freq
    mod2 = 'p2'
    mesh = 'example_2_mesh_coarse'
    P.import_slice_data(slicE, mod=mod2, mesh=mesh, key=t2,
                        stride=strd, freq=j)
    for flg in ['_E_t', '_H_t']:
        P.rel_errors(P.slice_data[t2 + flg],
                     P.slice_data['pyhed_ref_' + freq + flg])

    P.add_errors_to_slice_plot(
            ax, [0, j], str(0), comps[1], 'real', EH='E', e_lim=errlim,
            fs=fs, label=r'$\Re$($\mathbf{E}_y$) p1 coarse', tcolor='k')
    P.add_errors_to_slice_plot(
            ax, [1, j], str(2), comps[1], 'real', EH='E', e_lim=errlim,
            fs=fs, label=r'$\Re$($\mathbf{E}_y$) p1 fine', tcolor='k')
    P.add_errors_to_slice_plot(
            ax, [2, j], str(4), comps[1], 'real', EH='E', e_lim=errlim,
            fs=fs, label=r'$\Re$($\mathbf{E}_y$) p2 coarse', tcolor='k')

    P.add_errors_to_slice_plot(
            ax, [3, j], str(1), comps[2], 'imag', EH='H', e_lim=errlim,
            fs=fs, label=r'$\Im$($\mathbf{H}_z$) p1 coarse', tcolor='k')
    P.add_errors_to_slice_plot(
            ax, [4, j], str(3), comps[2], 'imag', EH='H', e_lim=errlim,
            fs=fs, label=r'$\Im$($\mathbf{H}_z$) p1 fine', tcolor='k')
    P.add_errors_to_slice_plot(
            ax, [5, j], str(5), comps[2], 'imag', EH='H', e_lim=errlim,
            fs=fs, label=r'$\Im$($\mathbf{H}_z$) p2 coarse', tcolor='k')

# make colorbar and adjust axes limits and apply log_scale to y-axis
print('done', j)
P.add_cbar(fig, errlim, cmap='RdBu_r', pos=[0.88, 0.2, 0.03, 0.6],
           label=r'$\mathbf{\epsilon}$ $[\%]$', label_pos=[0.25, 1.05],
           additional=1, symlog=True)

ticks = ax[0, -1].get_xticks()
for j in range(len(ax)):
    ax[j, 0].set_yticks(ticks)
    ax[j, 0].set_yticklabels(['-2', '0', '2'])

# save figure
# plt.savefig('./plots/errors_portrait.png', dpi=300, transparent=True,
#             bbox_inches='tight')

plt.show()  # uncomment to enable showing the plot
