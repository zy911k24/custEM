# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 2                          # # # # #
# ########################################################################### #
# # # # #                    mesh generation script                   # # # # #
# ########################################################################### #

from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.misc.synthetic_definitions import example_2_loop_tx
from custEM.meshgen import meshgen_utils as mu

## #########################       coarse mesh        ######################## #

# create world
M = BlankWorld(name='example_2_mesh_coarse',
               m_dir='./meshes',
               inner_area='box',                    # add observation area A
               inner_area_size=[2.1e3, 2.1e3],      # of 4.2 x 4.2 km size
               inner_area_cell_size=1000.,
               preserve_edges=True)

# add transmitter: crooked loop transmitter, defined in *synthetic_definitions*
M.build_surface(insert_loop_tx=[example_2_loop_tx()])

# build layered earth mesh and extend 2D surface mesh to 3D world
M.build_layered_earth_mesh(n_layers=3, layer_depths=[-300., -700.])

# extend the computational domain (reduce boundary artifacts for low freqs.)
M.there_is_always_a_bigger_world(x_fact=10., y_fact=10., z_fact=10.)

# call TetGen
M.call_tetgen(tet_param='-pq1.4aA', export_vtk=True)

# #########################    intermediate mesh     ######################## #

M = BlankWorld(name='example_2_mesh_inter',
               m_dir='./meshes',
               inner_area='box',
               inner_area_size=[2.1e3, 2.1e3],
               inner_area_cell_size=100.,
               preserve_edges=True)

M.build_surface(insert_loop_tx=[example_2_loop_tx()])
M.build_layered_earth_mesh(n_layers=3, layer_depths=[-300., -700.])
M.there_is_always_a_bigger_world(x_fact=10., y_fact=10., z_fact=10.)
M.call_tetgen(tet_param='-pq1.3aA', export_vtk=True)

# #########################        fine mesh         ######################## #

M = BlankWorld(name='example_2_mesh_fine',
               m_dir='./meshes',
               inner_area='box',
               inner_area_size=[2.1e3, 2.1e3],
               inner_area_cell_size=50.,
               preserve_edges=True)

M.build_surface(insert_loop_tx=[example_2_loop_tx()])
M.build_layered_earth_mesh(n_layers=3, layer_depths=[-300., -700.])
M.there_is_always_a_bigger_world(x_fact=10., y_fact=10., z_fact=10.)
M.call_tetgen(tet_param='-pq1.2aA', export_vtk=True)
