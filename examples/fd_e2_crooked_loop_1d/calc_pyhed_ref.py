# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import numpy as np
from custEM.misc import pyhed_calculations as pc
import os

# ########################################################################### #
# # # # #                          example 2                          # # # # #
# ########################################################################### #
# # # # #     calculate semi-analytic reference solution script       # # # # #
# ########################################################################### #

# arange identical coordinates to the ones of the interpolation slice
n_segs = 100
vec = np.linspace(-2e3, 2e3, n_segs + 1)
coords = np.zeros(((n_segs + 1)**2, 3))
a = 0
for j in range(n_segs + 1):
    for k in range(n_segs + 1):
        coords[a, 0] = vec[j]
        coords[a, 1] = vec[k]
        a += 1

# create reference solution directory
if not os.path.isdir('./ref_data'):
    os.makedirs('./ref_data')

# start the calculation of the reference solution using pyhed and save it
for freq in ['1', '10', '100', '1000']:
    Calculator = pc.PHC('./results/E_t/example_2_mesh_coarse_results/'
                        'example_2_p1_f_' + freq + '_config.json')
    Calculator.n_segs = 400
    for EH_flag in ['E', 'H']:
        data = Calculator.calc_reference(coords, EH_flag, max_procs=32)
        out_array = np.hstack((data, coords + 1j * coords))
        np.save('./ref_data/' + EH_flag + '_pyhed_freq_' + freq, out_array)
