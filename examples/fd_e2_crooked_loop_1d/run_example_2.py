# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 2                          # # # # #
# ########################################################################### #
# # # # #                      computation script                     # # # # #
# ########################################################################### #

from custEM.core import MOD
from custEM.misc.synthetic_definitions import example_2_loop_tx


# specify frequencies
frequencies = [1e0, 1e1, 1e2, 1e3]

# ########################## run p1 computations ############################ #

meshes = ['example_2_mesh_coarse',
          'example_2_mesh_inter',
          'example_2_mesh_fine']

for mesh in meshes:   # coarse, intermediate and fine meshes
    # Initialize MODel
    M = MOD('p1', mesh, 'E_t', p=1, overwrite_results=True,
            m_dir='./meshes', r_dir='./results')

    # define frequency and condcutivities
    M.MP.update_model_parameters(frequencies=frequencies,
                                  sigma_ground=[1e-3, 1e-2, 1e-4])

    # let FEniCS set up the variational formulations
    M.FE.build_var_form()  # in this example, the Tx information is passed
                            # automatically with the mesh parameters JSON file

    # call solver and convert H-fields
    M.solve_main_problem()

    M.IB.create_slice_mesh('z', dim=2e3, n_segs=100,  # 40 m Rx spacing
                           slice_name='surface')
    slicE = 'surface_slice_z'

    # interpolate fields on the observation slice
    M.IB.interpolate(slicE)

# ########################## run p2 computations ############################ #

# Initialize MODel
M = MOD('p2', 'example_2_mesh_coarse', 'E_t', p=2, overwrite_results=True,
        m_dir='./meshes', r_dir='./results')

# define frequency and condcutivities
M.MP.update_model_parameters(frequencies=frequencies,
                             sigma_ground=[1e-3, 1e-2, 1e-4])

# let FEniCS set up the variational formulations
M.FE.build_var_form()  # in this example, the Tx information is passed
                       # automatically with the mesh parameters JSON file.

# call solver and convert H-fields
M.solve_main_problem()

M.IB.create_slice_mesh('z', dim=2e3, n_segs=100,  # 40 m Rx spacing
                       slice_name='surface')
slicE = 'surface_slice_z'
# interpolate fields on the observation slice
M.IB.interpolate(slicE)
