# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 2                          # # # # #
# ########################################################################### #
# # # # #                    visualization script 1                   # # # # #
# ########################################################################### #

import numpy as np
from custEM.post import make_plain_subfigure_box
from custEM.post import PlotFD as Plot
import matplotlib.pyplot as plt
from matplotlib import rcParams

# define matplotlib parameters
# plt.ioff()
rcParams['axes.unicode_minus'] = False
rcParams['figure.figsize'] = 8, 13

EH_flg = 'H'
fs = 10
Elim = [1e-10, 1e-4]
Hlim = [1e-6, 1e-2]
errlim = [1e-1, 1e1]
strd = 1
comps = ['x', 'y', 'z', 'mag']
slicE = 'surface_slice_z'
mesh = 'example_2_mesh_coarse'
mod = 'p2'

# inititalize figure and Plot class
P = Plot(mod=mod, mesh=mesh, approach='E_t',
         r_dir='./results', s_dir='./plots')
P.import_slice_data(slicE, mod=mod, key='Tp2', stride=strd, freq=1)  # 10 Hz

data = np.load('./ref_data/E_pyhed_freq_10.npy')
data_s = P.reduce_slice_data(data, 3, 4, strd)
P.slice_data['ana_E_t'] = data_s[:, :3]
data = np.load('./ref_data/H_pyhed_freq_10.npy')
data_s = P.reduce_slice_data(data, 3, 4, strd)
P.slice_data['ana_H_t'] = data_s[:, :3]

# evaluate misfit to analytic solution
for flg in ['_E_t', '_H_t']:
    P.rel_errors(P.slice_data['Tp2' + flg],
                 P.slice_data['ana' + flg])

# plot data
fig, ax = make_plain_subfigure_box(6, 4, fs=fs, log_scale=False)
P.add_to_slice_plot(ax, [0, 0], 'x', 'real', key='Tp2', c_lim=Elim,
                    fs=fs, tcolor='k')
P.add_to_slice_plot(ax, [0, 1], 'x', 'real', key='ana', c_lim=Elim,
                    fs=fs, label=r'$\Re$($\mathbf{E}_x$) analytic', tcolor='k')
P.add_to_slice_plot(ax, [0, 2], 'x', 'imag', key='Tp2', c_lim=Elim,
                    fs=fs, tcolor='k')
P.add_to_slice_plot(ax, [0, 3], 'x', 'imag', key='ana', c_lim=Elim,
                    fs=fs, label=r'$\Im$($\mathbf{E}_x$) analytic', tcolor='k')

P.add_to_slice_plot(ax, [1, 0], 'y', 'real', key='Tp2', c_lim=Elim,
                    fs=fs, tcolor='k')
P.add_to_slice_plot(ax, [1, 1], 'y', 'real', key='ana', c_lim=Elim,
                    fs=fs, label=r'$\Re$($\mathbf{E}_y$) analytic', tcolor='k')
P.add_to_slice_plot(ax, [1, 2], 'y', 'imag', key='Tp2', c_lim=Elim,
                    fs=fs, tcolor='k')
P.add_to_slice_plot(ax, [1, 3], 'y', 'imag', key='ana', c_lim=Elim,
                    fs=fs, label=r'$\Im$($\mathbf{E}_y$) analytic', tcolor='k')

# make colorbar for E-fields
P.add_cbar(fig, Elim, pos=[0.88, 0.64, 0.03, 0.22], fs=10,
           additional=1, label=r'$\mathbf{E}$ $[V/m]$', label_pos=[0.2, 1.05])
P.add_to_slice_plot(ax, [2, 0], 'x', 'real', key='Tp2', c_lim=Hlim,
                    EH='H', fs=fs, tcolor='k')
P.add_to_slice_plot(ax, [2, 1], 'x', 'real', key='ana', c_lim=Hlim, tcolor='k',
                    EH='H', fs=fs, label=r'$\Re$($\mathbf{H}_x$) analytic')
P.add_to_slice_plot(ax, [2, 2], 'x', 'imag', key='Tp2', c_lim=Hlim,
                    EH='H', fs=fs, tcolor='k')
P.add_to_slice_plot(ax, [2, 3], 'x', 'imag', key='ana', c_lim=Hlim, tcolor='k',
                    EH='H', fs=fs, label=r'$\Im$($\mathbf{H}_x$) analytic')

P.add_to_slice_plot(ax, [3, 0], 'y', 'real', key='Tp2', c_lim=Hlim,
                    EH='H', fs=fs, tcolor='k')
P.add_to_slice_plot(ax, [3, 1], 'y', 'real', key='ana', c_lim=Hlim, tcolor='k',
                    EH='H', fs=fs, label=r'$\Re$($\mathbf{H}_y$) analytic')
P.add_to_slice_plot(ax, [3, 2], 'y', 'imag', key='Tp2', c_lim=Hlim,
                    EH='H', fs=fs, tcolor='k')
P.add_to_slice_plot(ax, [3, 3], 'y', 'imag', key='ana', c_lim=Hlim, tcolor='k',
                    EH='H', fs=fs, label=r'$\Im$($\mathbf{H}_y$) analytic')

P.add_to_slice_plot(ax, [4, 0], 'z', 'real', key='Tp2', c_lim=Hlim,
                    EH='H', fs=fs, tcolor='k')
P.add_to_slice_plot(ax, [4, 1], 'z', 'real', key='ana', c_lim=Hlim, tcolor='k',
                    EH='H', fs=fs, label=r'$\Re$($\mathbf{H}_z$) analytic')
P.add_to_slice_plot(ax, [4, 2], 'z', 'imag', key='Tp2', c_lim=Hlim,
                    EH='H', fs=fs, tcolor='k')
P.add_to_slice_plot(ax, [4, 3], 'z', 'imag', key='ana', c_lim=Hlim, tcolor='k',
                    EH='H', fs=fs, label=r'$\Im$($\mathbf{H}_z$) analytic')

# make colorbar for H-fields
P.add_cbar(fig, Hlim, cmap='viridis', pos=[0.88, 0.375, 0.03, 0.22], fs=10,
           additional=1,
           label=r'$\mathbf{H}$ $[A/m]$', label_pos=[0.2, 1.05])
P.add_errors_to_slice_plot(ax, [5, 0], str(0), comp='mag',
                           ri='real', EH='E', e_lim=errlim, tcolor='k',
                           fs=fs, label=r'$\Re$($\mathbf{|E|}$) misfit')
P.add_errors_to_slice_plot(ax, [5, 1], str(0), comp='mag',
                           ri='imag', EH='E', e_lim=errlim, tcolor='k',
                           fs=fs, label=r'$\Im$($\mathbf{|E|}$) misfit')
P.add_errors_to_slice_plot(ax, [5, 2], str(1), comp='mag',
                           ri='real', EH='H', e_lim=errlim, tcolor='k',
                           fs=fs, label=r'$\Re$($\mathbf{|H|}$) misfit')
P.add_errors_to_slice_plot(ax, [5, 3], str(1), comp='mag',
                           ri='imag', EH='H', e_lim=errlim, tcolor='k',
                           fs=fs, label=r'$\Im$($\mathbf{|H|}$) misfit')

# make colorbar for misfits and adjust axes ticks
P.add_cbar(fig, errlim, cmap='RdBu_r', pos=[0.88, 0.11, 0.03, 0.22],
           fs=10, additional=1, symlog=True,
           label=r'$\mathbf{\epsilon}$ $[\%]$', label_pos=[0.25, 1.05])
ticks = ax[0, -1].get_xticks()
for j in range(len(ax)):
    ax[j, 0].set_yticks(ticks)
    ax[j, 0].set_yticklabels(['-2', '0', '2'])

# save figure
# plt.savefig('./plots/10_Hz_p2.png', dpi=600, transparent=True,
#             bbox_inches='tight')
# plt.ion()   # uncomment to enable showing the plot
plt.show()  # uncomment to enable showing the plot
