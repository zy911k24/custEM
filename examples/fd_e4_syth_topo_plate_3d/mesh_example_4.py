# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 4                          # # # # #
# ########################################################################### #
# # # # #                    mesh generation script                   # # # # #
# ########################################################################### #

from custEM.meshgen.meshgen_utils import loop_r
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.misc.synthetic_definitions import example_4_topo_cos_sin
from custEM.misc.synthetic_definitions import surface_anomly_outcrop_3


# init world and set up fine discretization in central area
M = BlankWorld(name='example_4_mesh', m_dir='./meshes',
               inner_area_size=[2.1e3, 2.1e3],
               inner_area_cell_size=2e4,
               outer_area_cell_size=1e6,
               inner_area='box',
               topo=example_4_topo_cos_sin,       # define (synth.) topo
               preserve_edges=True)

# build surface mesh and incorporate anomaly outcrop
M.build_surface(insert_loop_tx=[loop_r(start=[-5e2, -5e2],
                                       stop=[5e2, 5e2],
                                       n_segs=201)])

# add the dipping plate anomaly described as polygone at the surface
M.add_surface_anomaly(insert_paths=[surface_anomly_outcrop_3()],
                      dips=[70.],
                      depths=[-3000.],
                      dip_azimuths=[10.],
                      cell_sizes=[1e5],
                      marker_positions=[[-140., -475., -260.]])

# extend 2D surface mesh to 3D world
M.build_halfspace_mesh()

# call TetGen and use 'M'-flag to preserve the loop Tx
M.call_tetgen(tet_param='-pq1.4aA', export_vtk=True)
