﻿################################################################################
# # # # #                 frequency-domain IP Example                  # # # # #
################################################################################

  Induced polarization (IP) modeling of 1D layered-earth setup and validation
  against empymod.
  
  The reference data are computed on-the-fly in the plot script and require
  empymod to be installed (e.e., use "conda install empymod" in the custEM
  environment)
  
  The handling of IP parameters in 3D is equivalent to setting conductivities
  for all domains in the mesh as shown in the other examples. Note that
  anisotropic IP parameters 
  
################################################################################

    - Start with generating the mesh by running the *mesh_ip_layered.py* file:

        > python mesh_ip_layered.py
    
    - The *run_ip_layered.py* script should be called in parallel:

        > mpirun -n 24 python -u run_ip_layered.py (and export OMP_NUM_THERADS=2,
                                                    recommended for p2)
    
    - The computation will requie less than 64 GB RAM with this 
      parallelization configuration.  
    
    - The *plot_mt_dublin.py* script can be used to compare the different 
	  reference data from www.mtnet.info with the custEM results

        > python plot_mt_dublin.py 

################################################################################