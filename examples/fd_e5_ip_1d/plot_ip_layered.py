# coding: utf-8

# Plot Aubure line

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
from custEM.post import PlotFD as Plot
from custEM.post import make_plain_subfigure_box
import empymod


def cole_cole(inp, p_dict):
    """Cole and Cole (1941)."""

    # Compute complex conductivity from Cole-Cole
    iotc = np.outer(2j*np.pi*p_dict['freq'], inp['tau'])**inp['c']
    # condH = inp['cond_8'] + (inp['cond_0']-inp['cond_8'])/(1+iotc)
    # condV = condH/p_dict['aniso']**2

    condH = inp['cond_0'] / (1 - inp['m'] * (1 - 1/(1+iotc)))
    condV = condH

    # Add electric permittivity contribution
    etaH = condH + 1j*p_dict['etaH'].imag
    etaV = condV + 1j*p_dict['etaV'].imag

    return etaH, etaV

def pelton(inp, p_dict):
    """ Pelton et al. (1978)."""

    # Compute complex resistivity from Pelton et al.
    iotc = np.outer(2j*np.pi*p_dict['freq'], inp['tau'])**inp['c']
    rhoH = inp['rho_0']*(1 - inp['m']*(1 - 1/(1 + iotc)))
    rhoV = rhoH*p_dict['aniso']**2

    # Add electric permittivity contribution
    etaH = 1/rhoH + 1j*0. #p_dict['etaH'].imag
    etaV = 1/rhoV + 1j*0. #p_dict['etaV'].imag

    return etaH, etaV

# define empymod model

cc = plt.rcParams['axes.prop_cycle'].by_key()['color']
plt.rcParams['axes.titlepad'] = 4
plt.rcParams['axes.unicode_minus'] = False
plt.rcParams['axes.edgecolor'] = '#002C72'
plt.rcParams['xtick.color'] = '#002C72'
plt.rcParams['ytick.color'] = '#002C72'
plt.rcParams['axes.labelcolor'] = '#002C72'

fig = plt.figure(figsize=(12, 6))
gs = plt.GridSpec(2, 1)
ax0 = fig.add_subplot(gs[0])
plt.grid()
ax1 = fig.add_subplot(gs[1])
plt.grid()
freqs = np.logspace(0, 4, 5)

EH = 'H'
ri = 'mag'
tx = 1   # 0 = dipole, 1 = loop

for fi, fs in enumerate(
        ['1', '10', '100', '1000', '10000']):
    xrec = np.linspace(0., 200., 21)

    if EH == 'H':
        model = {
            'rec': [xrec, np.ones(xrec.size) * 100., 0.01, 0, 0],
            'depth': [0., 100., 200.],
            'freqtime': freqs[fi],  # Hz
            'srcpts': 11,
            'mrec': True,
        }
    else:
        model = {
            'rec': [xrec, np.ones(xrec.size) * 100., 0.01, 90, 0],
            'depth': [0., 100., 200.],
            'freqtime': freqs[fi],  # Hz
            'srcpts': 11,
        }

    res = np.array([1e8, 1e2, 1e1, 1e3])
    c = np.array([0., 0.8, 1., 0.])
    m = np.array([0., 0.7, 0.5, 0.])
    tau = np.array([0., 1., 0.1, 0.])

    res_8 = m * res - res

    # calculate cole cole model with empymod

    cole_model = {'res': res, 'cond_0': 1/res, 'm': m, 'cond_8': 1/res_8,
                  'tau': tau, 'c': c, 'func_eta': cole_cole}
    pelton_model = {'res': res, 'rho_0': res, 'm': m,
                    'tau': tau, 'c': c, 'func_eta': pelton}

    if tx == 0:
        ref = empymod.bipole(src=[-200, -200., -100., 100., 0.01, 0.01],
                             res=cole_model, **model)
    elif tx == 1:
        ref = empymod.bipole(src=[-300, -100., -100., -100., 0.01, 0.01],
                                  res=cole_model, **model)
        ref += empymod.bipole(src=[-100, -100., -100., 100., 0.01, 0.01],
                              res=cole_model, **model)
        ref += empymod.bipole(src=[-100, -300., 100., 100., 0.01, 0.01],
                              res=cole_model, **model)
        ref += empymod.bipole(src=[-300, -300., 100., -100., 0.01, 0.01],
                              res=cole_model, **model)

    cu = np.load('results/E_t/ip_3l/' +
                  'p2_new_interpolated/f_' + str(fi) + '_' + EH +
                  '_t_all_tx_rx_path.npy')[tx][:, 0] / 200.

    # if EH == 'H':
    #     cu = np.load('results/E_t/ip_3l_fine/' +
    #                   'p2_new_interpolated/f_' + str(fi) + '_tx_0_' + EH + '_t_obs_line_x.npy')
    #     cu = cu[:, 0][::-1] / 200.
    # else:
    #     cu = np.load('results/E_t/ip_3l_fine/' +
    #                   'p2_new_interpolated/f_' + str(fi) + '_tx_0_' + EH + '_t_obs_line_x.npy')
    #     cu = cu[:, 1][::-1] / 200.

    if ri == 'real':
        cu1, ref1 = cu.real, ref.real
        st = '\Re'
    elif ri == 'imag':
        cu1, ref1 = cu.imag, ref.imag
        st = '\Im'
    else:
        cu1, ref1 = np.abs(cu), np.abs(ref)
        st = ''
    misfit = np.abs(np.abs(cu1) - np.abs(ref1)) / np.abs(ref1) * 100

    if fi == 0:
        ax0.plot(xrec, np.abs(ref1), ':k', lw=0.5, label='empymod')
    else:
        ax0.plot(xrec, np.abs(ref1), ':k', lw=0.5)
    ax0.plot(xrec, np.abs(cu1), '^', color=cc[fi], ms=2.,
             label='custEM ' + fs + ' Hz')

    ax1.plot(xrec, misfit, '^:', color=cc[fi], ms=2., lw=0.5)

ax0.legend(labelcolor='#002C72')
ax0.set_yscale('log')
#ax0.set_ylim([1e-10, 1e-5])
ax0.set_xlim([0., 200.])
ax0.set_xlabel('x (m)')


ax1.set_yscale('log')
ax1.set_ylim([1e-2, 1e2])
ax1.set_xlim([0., 200.])
ax1.set_xlabel('x (m)')

# if EH == 'H':
#     ax0.set_ylabel(r'||$\mathbf{' + EH + '}_x$|| (A/m)')
#     ax1.set_ylabel(r'$\epsilon_{||\mathbf{' + EH + '}_x||}$ (%)')
# else:
#     ax0.set_ylabel(r'||$\mathbf{' + EH + '}_y$|| (V/m)')
#     ax1.set_ylabel(r'$\epsilon_{||\mathbf{' + EH + '}_y||}$ (%)')

ax1.set_ylabel(r'$\epsilon_{||\mathbf{' + EH + '}_x||}$ (%)')
if EH == 'H':
    ax0.set_ylabel(r'||($\mathbf{' + EH + '}_x$)| (A/m)')
else:
    ax0.set_ylabel(r'||($\mathbf{' + EH + '}_x$)| (V/m)')

plt.savefig('Line_Tx_ip_' + EH + '.png', bbox_inches='tight', dpi=300)

plt.show()
