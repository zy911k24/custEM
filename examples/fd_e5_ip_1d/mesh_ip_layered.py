# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import numpy as np
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld

rx = np.zeros((21, 3))

rx[:, 0] = np.linspace(0., 200., 21)
rx[:, 1] = 100.
rx[:, 2] = -0.1
rx_tri = mu.refine_rx(rx, 1., 30.)

dim = 3e3
M = BlankWorld(name='ip_3l',
               x_dim=[-dim, dim],
               y_dim=[-dim, dim],
               z_dim=[-dim, dim],
               m_dir='./meshes',
               preserve_edges=True,
               preserve_nodes=True,
               )

M.build_surface(insert_line_tx=[mu.line_y(-100., 100., x=-200., n_segs=49)],
                insert_loop_tx=[mu.loop_r([-300., -100.], [-100., 100.],
                                          n_segs=196)],
                insert_paths=rx_tri)

M.build_layered_earth_mesh(3, [-100., -200.])

M.add_rx(rx)

M.extend_world(10., 10., 10.)
M.call_tetgen(tet_param='-pq1.6aA', export_vtk=True)
