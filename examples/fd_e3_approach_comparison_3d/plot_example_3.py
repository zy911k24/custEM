# -*- coding: utf-8 -*-
"""
@author:  Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 3                          # # # # #
# ########################################################################### #
# # # # #                     visualization script                    # # # # #
# ########################################################################### #

import matplotlib.pyplot as plt
from matplotlib import rcParams
from custEM.post import PlotFD as Plot
from matplotlib import colors as cl
from matplotlib.colors import LogNorm
from matplotlib.pyplot import rcParams as rc
import numpy as np

# define matplotlib parameters and model, mesh and key names
rcParams['axes.unicode_minus'] = False
rcParams['figure.figsize'] = 7, 9
clrs = cl.CSS4_COLORS

p = 2
tx = 1  # 0 - dipole tx, 1 - loop tx
linE = 'surface_line_x'
#linE = 'air_line_x'

mesh = 'example_3_mesh_p' + str(p)
mesh = 'example_3_mesh_p' + str(p)

modE = 'p' + str(p) + '_pf_E'
modH = 'p' + str(p) + '_pf_H'

t1, t2, s1, s2, s3, s4, s5 = 'T1', 'T2', 'S1', 'S2', 'S3', 'S4', 'S5'

# ############################## import data ################################ #

P = Plot(mod=modE, mesh=mesh, approach='E_t',
         r_dir='./results', s_dir='./plots', label_color='#002C72')

P.import_line_data(linE, mesh=mesh, mod=modE, approach='E_t', tx=tx, key=t1)
P.import_line_data(linE, mesh=mesh, mod=modE, approach='Am_t', tx=tx, key=t2)
P.import_line_data(linE, mesh=mesh, mod=modH, approach='E_s', tx=tx, key=s1)
P.import_line_data(linE, mesh=mesh, mod=modH, approach='H_s', tx=tx, key=s2)
P.import_line_data(linE, mesh=mesh, mod=modH, approach='Am_s', tx=tx, key=s3)
P.import_line_data(linE, mesh=mesh, mod=modH, approach='An_s', tx=tx, key=s4)
P.import_line_data(linE, mesh=mesh, mod=modH, approach='Fm_s', tx=tx, key=s5)

# ########################### plot total fields ############################# #

EH = 'EH'
P.plot_line_data(key=t1, EH=EH, label='E_t_p1', lw=1., ls='-',
                 color='b', ylim=[1e-10, 1e-2])
P.plot_line_data(key=t2, EH=EH, new=False, label='Am_t_p1',
                 lw=1.5, ls=':', color=clrs['orange'])
P.plot_line_data(key=s1, EH=EH, new=False, label='E_s_p1',
                 lw=0.5, ls='-', color='c')
P.plot_line_data(key=s2, EH=EH, new=False, label='H_s_p1',
                 lw=0.5, ls=':', color='m')
P.plot_line_data(key=s3, EH=EH, new=False, label='Am_s_p1',
                 lw=0.5, ls='-', color='k')
P.plot_line_data(key=s4, EH=EH, new=False, label='An_s_p1',
                 lw=1., ls=':', color='r')
P.plot_line_data(key=s5, EH=EH, new=False, label='Tm_s_p1',
                 lw=1., ls=':', color='c')
plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.1,
                    hspace=0.14)
plt.savefig('./plots/p1.pdf', bbox_inches='tight', pad_inches=0)

# ######################### plot secondary fields ########################### #

#P.plot_line_data(key=s1, EH=EH, label='E_s_p1', ylim=[1e-10, 1e-2],
#                 lw=0.5, ls='-', color='c', sf=True)
##P.plot_line_data(key=s2, EH=EH, new=False, label='H_s_p1',
##                 lw=0.5, ls='-', color='g', sf=True)
#P.plot_line_data(key=s3, EH=EH, new=False, label='Am_s_p1',
#                 lw=1., ls=':', color='k', sf=True)
#P.plot_line_data(key=s4, EH=EH, new=False, label='An_s_p1',
#                 lw=0.5, ls=':', color='m', sf=True)
#P.plot_line_data(key=s5, EH=EH, new=False, label='Tm_s_p1',
#                 lw=1., ls=':', color='r', sf=True)
#P.plot_line_data(key=p2s2, EH=EH, new=False, label='H_SF_p2', lw=0.5,
#                 ls='-', color='k', sf=True)
#P.plot_line_data(key=p2s1, EH=EH, new=False, label='E_SF_p2', lw=0.5,
#                 ls=':', color='g', sf=True)
#plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.1,
#                    hspace=0.14)
#plt.savefig('./plots/sf_p12.pdf', transparent=True, bbox_inches='tight')

plt.show()  # uncomment to enable showing the plot

