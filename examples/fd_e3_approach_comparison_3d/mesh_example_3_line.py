# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 3                          # # # # #
# ########################################################################### #
# # # # #                    mesh generation script                   # # # # #
# ########################################################################### #

from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld

rx_p1 = mu.refine_rx(mu.line_x(start=-5e3, stop=5e3, n_segs=200), 1.1, 30.)
rx_p2 = mu.refine_rx(mu.line_x(start=-5e3, stop=5e3, n_segs=200), 20., 30.)

# ######################### mesh for p1 computations ######################## #

# create world
M = BlankWorld(name='example_3_mesh_p1_line', m_dir='./meshes',
               airspace_cell_size=1e8,
               subsurface_cell_size=1e8,
               preserve_edges=True)

# add transmitter: 1 km y-directed dipole and 10 km x-directed observation line
M.build_surface(insert_lines=[mu.line_y(start=-5e2, stop=5e2, n_segs=100)],
                insert_paths=rx_p1)

# build halfspace mesh and extend 2D surface mesh to 3D world
M.build_halfspace_mesh()

# add the dipping plate anomaly
M.add_plate(1000., 1000., 100., [500., 100., -700.], 45., 117., cell_size=1e3)

# add the brick anomaly for which an anisotropic conductivity is set later on
M.add_brick(start=[-1000., -300., -700.],
            stop=[-500.0, 700.0, -200.],
            cell_size=1e3)

# extend the computational domain (reduce boundary artifacts for low freqs.)
M.there_is_always_a_bigger_world(x_fact=1e1, y_fact=1e1, z_fact=1e1)

# call TetGen
M.call_tetgen(tet_param='-pq1.2aA', export_vtk=True)
#
# ########################## mesh for p2 computations ####################### #

M = BlankWorld(name='example_3_mesh_p2_line', m_dir='./meshes',
               preserve_edges=True)

M.build_surface(insert_lines=[mu.line_y(start=-5e2, stop=5e2, n_segs=50)],
                insert_paths=rx_p2)
M.build_halfspace_mesh()
M.add_plate(1000., 1000., 100., [500., 100., -700.], 45., 117., cell_size=1e4)
M.add_brick(start=[-1000., -300., -700.],
            stop=[-500.0, 700.0, -200.],
            cell_size=1e4)
M.there_is_always_a_bigger_world(x_fact=1e1, y_fact=1e1, z_fact=1e1)
M.call_tetgen(tet_param='-pq1.4aA', export_vtk=True)
