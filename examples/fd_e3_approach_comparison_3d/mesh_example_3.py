# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 3                          # # # # #
# ########################################################################### #
# # # # #                    mesh generation script                   # # # # #
# ########################################################################### #

#

from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld

rx_p1 = mu.refine_rx(mu.line_x(start=-5e3, stop=5e3, n_segs=200), 1., 30.)
rx_p2 = mu.refine_rx(mu.line_x(start=-5e3, stop=5e3, n_segs=200), 20., 30.)

del rx_p1[110]  # avoid tx-rx intersection at (500, 0, 0 )
del rx_p2[110]  # avoid tx-rx intersection at (500, 0, 0 )
del rx_p1[100]  # avoid tx-rx intersection at (0, 0, 0 )
del rx_p2[100]  # avoid tx-rx intersection at (0, 0, 0 )
del rx_p1[90]  # avoid tx-rx intersection at (-500, 0, 0 )
del rx_p2[90]  # avoid tx-rx intersection at (-500, 0, 0 )

# ######################### mesh for p1 computations ######################## #

# create world
M = BlankWorld(name='example_3_mesh_p1', m_dir='./meshes',
                airspace_cell_size=1e8,
                subsurface_cell_size=1e8,
                preserve_edges=True)

# add transmitters and refined receiver positions:
# first tx: 1 km y-directed grounded line transmitter
# second tx: 1 x 1 km rectangular ungrounded loop transmitter
# rx: 10 km x-directed observation line with 50 m spacing
M.build_surface(insert_line_tx=[mu.line_y(start=-5e2, stop=5e2, n_segs=41)],
                insert_loop_tx=[mu.loop_r([-5e2, -5e2], [5e2, 5e2],
                                          n_segs=160)],
                insert_paths=rx_p1)

# build halfspace mesh and extend 2D surface mesh to 3D world
M.build_halfspace_mesh()

# add the first dipping plate anomaly
M.add_plate(1000., 1000., 100., [500., 100., -700.], 45., 117., cell_size=1e3)

# add the second brick anomaly
M.add_brick(start=[-1000., -300., -700.],
            stop=[-500.0, 700.0, -200.],
            cell_size=1e3)

# extend the computational domain (reduce boundary artifacts for low freqs.)
M.there_is_always_a_bigger_world(x_fact=1e1, y_fact=1e1, z_fact=1e1)

# call TetGen, recommend quality=1.2 for p1 computations
M.call_tetgen(tet_param='-pq1.2aA', export_vtk=True)

# ########################## mesh for p2 computations ####################### #

M = BlankWorld(name='example_3_mesh_p2', m_dir='./meshes',
               preserve_edges=True)

M.build_surface(insert_line_tx=[mu.line_y(start=-5e2, stop=5e2, n_segs=21)],
                insert_loop_tx=[mu.loop_r([-5e2, -5e2], [5e2, 5e2],
                                          n_segs=80)],
                insert_paths=rx_p2)
M.build_halfspace_mesh()
M.add_plate(1000., 1000., 100., [500., 100., -700.], 45., 117., cell_size=1e4)
M.add_brick(start=[-1000., -300., -700.],
            stop=[-500.0, 700.0, -200.],
            cell_size=2e4)
M.there_is_always_a_bigger_world(x_fact=1e1, y_fact=1e1, z_fact=1e1)

# recommended quality=1.4-1.6 for p2
M.call_tetgen(tet_param='-pq1.6aA', export_vtk=True)
