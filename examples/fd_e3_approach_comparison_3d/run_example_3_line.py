# -*- coding: utf-8 -*-
"""
@author:  Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 3                          # # # # #
# ########################################################################### #
# # # # #                      computation script                     # # # # #
# ########################################################################### #

from custEM.core import MOD
import numpy as np
import dolfin as df


approaches = ['E_t', 'E_s', 'H_s', 'Am_t', 'Am_s', 'An_t', 'An_s', 'Fm_s']
omega = 1e1 * 2. * np.pi

pf_EH = 'E'
linE1 = 'x0_-5000.0_x1_5000.0_y_0.0_z_-0.1_n_200_line_x'
linE2 = 'x0_-5000.0_x1_5000.0_y_0.0_z_50.0_n_200_line_x'

# ########################## run p1 computations ############################ #

p = 1
mod = 'example_3_p1_aniso' + pf_EH
mesh = 'example_3_mesh_p1_line'

for approach in approaches:       # all approaches

    # initialize MODel
    M = MOD(mod, mesh, approach, p=p,  overwrite_results=True,
            serial_ordering=True, m_dir='./meshes', r_dir='./results')

    # define frequency and conductivities
    M.MP.update_model_parameters(omega=omega,
                                 sigma_ground=[1e-3, 2e-1, [0.1, 0.1, 1.]],
                                 sigma_0=[1e-3, 1e-3, 1e-3])

    # 1000 m dipole along y-axis
    M.FE.build_var_form(s_type='line',
                        start=[0., -5e2, 0.],
                        stop=[0., 5e2, 0.],
                        n_segs=101,
                        pf_EH_flag=pf_EH)

    # call solver and convert H-fields
    M.solve_main_problem(convert_to_E=True)

    # create regular 5 km inteprolation line in x-dir at surface
    M.IB.create_line_mesh('x',  -5e3, 5e3, 200, z=-0.1)
    M.IB.create_line_mesh('x',  -5e3, 5e3, n_segs=200, z=50.)

    # interpolate fields on the observation line
    if '_s' in approach:   # if secondary field formulation used
        quantities = ['H_t', 'E_t', 'H_s', 'E_s']
    else:                  # if total field formulation used
        quantities = ['H_t', 'E_t']

    M.IB.interpolate([linE1, linE2], quantities)

# ########################## run p2 computations ############################ #

p = 2
pf_EH = 'E'
mod = 'example_3_p2_aniso' + pf_EH
mesh = 'example_3_mesh_p2_line'

for approach in approaches:

    # initialize MODel
    M = MOD(mod, mesh, approach, p=p, overwrite_results=True,
            serial_ordering=True, m_dir='./meshes', r_dir='./results')

    # define frequency and conductivities
    M.MP.update_model_parameters(omega=omega,
                                 sigma_ground=[1e-3, 2e-1, [0.1, 0.1, 1.]],
                                 sigma_0=[1e-3, 1e-3, 1e-3])

    # 1000 m dipole along y-axis
    M.FE.build_var_form(s_type='line',
                        start=[0., -5e2, 0.],
                        stop=[0., 5e2, 0.],
                        n_segs=51,
                        pf_EH_flag=pf_EH)

    # call solver and convert H-fields
    M.solve_main_problem(convert_to_E=True)

    # create regular 5 km inteprolation line in x-dir at surface
    M.IB.create_line_mesh('x',  -5e3, 5e3, 200, z=-0.1)
    M.IB.create_line_mesh('x',  -5e3, 5e3, n_segs=200, z=50.)

    # interpolate fields on the observation line
    if '_s' in approach:   # if secondary field formulation used
        quantities = ['H_t', 'E_t', 'H_s', 'E_s']
    else:                  # if total field formulation used
        quantities = ['H_t', 'E_t']

    M.IB.interpolate([linE1, linE2], quantities)
