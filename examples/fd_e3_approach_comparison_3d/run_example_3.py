# -*- coding: utf-8 -*-
"""
@author:  Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 3                          # # # # #
# ########################################################################### #
# # # # #                      computation script                     # # # # #
# ########################################################################### #

# In this example, all implemented frequency-domain approaches for CSEM
# modeling are compared against each other for a line and loop source
# configuration (automatically identified from the transmitters incorporated
# in the mesh - see *mesh_example_3.py*); the model includes a steep 3D dipping
# plate (1st anomaly) and a vertical-transverse isotropic brick anomaly (2nd
# anomaly) in a 100 Ohmm halfspace.

# Notice: in contrast to the original version of this example in custEM
# versions 0.93-0.98, the experimental total A-V nodal formulation was removed
# as it does not produce robust results with the tested approach of
# implementing the source currents on the nodes.


from custEM.core import MOD
import numpy as np
import dolfin as df

approaches = ['E_t', 'E_s', 'H_s', 'Am_t', 'Am_s', 'An_s', 'Fm_s']
approaches = ['E_s', 'H_s', 'Am_s', 'An_s', 'Fm_s']
pf_EH = 'H'  # choose *E* or *H* for corresponding primary fields in the
# secondary-field appraoches

# ############################  run computations  ########################### #

p = [1, 2]
meshes = ['example_3_mesh_p1', 'example_3_mesh_p2']
dummy = True

for jj, mesh in enumerate(meshes):
    mod = 'p' + str(p[jj]) + '_pf_' + pf_EH
    for approach in approaches:       # all approaches

        # Initialize MODel
        M = MOD(mod, mesh, approach, p=p[jj], overwrite=True,
                m_dir='./meshes', r_dir='./results')

        # define frequency and conductivities
        M.MP.update_model_parameters(f=10.,
                                     sigma_ground=[1e-3, 2e-1,
                                                   [1e-1, 1e-1, 1.]],
                                     sigma_0=[1e-3, 1e-3, 1e-3])

        # build variational formulation
        M.FE.build_var_form()  # in this example, the Tx information is passed
                               # automatically with the mesh parameters file

        # Call solver and convert H-fields
        M.solve_main_problem(convert_to_E=True)

        # create regular 5 km inteprolation line in x-dir at surface
        M.IB.create_line_meshes('x',  x0=-5e3, x1=5e3, n_segs=200,
                                line_name='surface')
        M.IB.create_line_meshes('x',  x0=-5e3, x1=5e3, z=50., n_segs=200,
                                line_name='air')
        dummy = False

        # interpolate fields on the observation line
        if '_s' in approach:   # if secondary field formulation used
            quantities = ['H_t', 'E_t', 'H_s', 'E_s']
        else:                  # if total field formulation used
            quantities = ['H_t', 'E_t']

        M.IB.interpolate(['surface_line_x', 'air_line_x'], quantities)
        M.IB.synchronize()
