import math
import cmath
import time
import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt


def snrms(data1, data2):

    return(200 * (np.abs(np.abs(data1) - np.abs(data2)) /
           (np.abs(data1) + np.abs(data2))))


def extract_lines_from_ref(ref):

    refs, yref, xref1, xref2, xref3 = [], [], [], [], []

    k = 0
    for j in range(59):
        refs.append(ref[k:k+21])
        if refs[-1][0, 1] == 0.:
            yref.append(ref[k:k+21])
        if refs[-1][0, 0] == -15e3 and refs[-1][0, 1] != 0.:
            xref1.append(ref[k:k+21])
        if refs[-1][0, 0] == 0. and refs[-1][0, 1] != 0.:
            xref2.append(ref[k:k+21])
        if refs[-1][0, 0] == 15e3 and refs[-1][0, 1] != 0.:
            xref3.append(ref[k:k+21])
        k += 21

    return(np.array(yref), np.array(xref1), np.array(xref2), np.array(xref3))


cc = plt.rcParams['axes.prop_cycle'].by_key()['color']

plt.rcParams['axes.titlepad'] = 4
plt.rcParams['axes.unicode_minus'] = False
plt.rcParams['axes.edgecolor'] = '#002C72'
plt.rcParams['xtick.color'] = '#002C72'
plt.rcParams['ytick.color'] = '#002C72'
plt.rcParams['axes.labelcolor'] = '#002C72'

fig = plt.figure(figsize=(8, 6))
gs = plt.GridSpec(2, 2)


ax0 = fig.add_subplot(gs[0])
plt.grid()
ax1 = fig.add_subplot(gs[2])
plt.grid()
ax2 = fig.add_subplot(gs[1])
plt.grid()
ax3 = fig.add_subplot(gs[3])
plt.grid()

# ########################################################################### #

periods = [0.1, 0.18, 0.32, 0.56, 1., 1.8, 3.2, 5.6,
           10., 18., 32., 56., 100., 180., 320., 560.,
           1000., 1800., 3200., 5600., 10000.]
freqs = 1. / np.array(periods)

fi = 0
lint = 0
lint2 = 3
ap = True

if not ap:
    switch = 'Z'
else:
    switch = 'RP'

# # choose one or more of the following reference data
# ref_mod = 'Mackie FD'
# ref_mod = 'Miensopust_mt3dinv FD'
# ref_mod = 'Miensopust_wsinv3dmt FD'
# ref_mod = 'Han_and_Lee FE'

ref_mods = ['Mackie FD', 'Han_and_Lee FE']

ref = []
yref, xref1, xref2, xref3 = [], [], [], []
for ref_mod in ref_mods:
    ref.append(np.loadtxt(
        'refdata/' + switch + '_' + ref_mod[:-3] + '.dat', skiprows=1))
    y, x1, x2, x3 = extract_lines_from_ref(ref[-1])
    yref.append(y)
    xref1.append(x1)
    xref2.append(x2)
    xref3.append(x3)

custem_rdir = 'results/MT/dublin/p1_interpolated/'
Z = np.load(custem_rdir  + 'f_' + str(fi) + '_Z_rx_path_' +
            str(lint2) + '.npy')
rhoa = np.load(custem_rdir  + 'f_' + str(fi) + '_R_rx_path_' +
               str(lint2) + '.npy')
phase = np.load(custem_rdir  + 'f_' + str(fi) + '_P_rx_path_' +
                str(lint2) + '.npy')

style = ['ok', 'xk']

if not ap:
    misfit1 = snrms(Z[:, 0, 1].real, ref[:, fi, 5])
    misfit2 = snrms(Z[:, 1, 0].real, ref[:, fi, 7])
    misfit3 = snrms(Z[:, 0, 1].imag, ref[:, fi, 6])
    misfit4 = snrms(Z[:, 1, 0].imag, ref[:, fi, 8])

    misfit5 = snrms(Z[:, 0, 0].real, ref[:, fi, 3])
    misfit6 = snrms(Z[:, 1, 1].real, ref[:, fi, 9])
    misfit7 = snrms(Z[:, 0, 0].imag, ref[:, fi, 4])
    misfit8 = snrms(Z[:, 1, 1].imag, ref[:, fi, 10])
    ax0.plot(vec, np.abs(Z[:, 0, 1].real), '-b', label='cu Zxy')
    ax0.plot(vec, np.abs(Z[:, 1, 0].real), '-c', label='cu Zyx')
    ax2.plot(vec, np.abs(Z[:, 0, 1].imag), '-b', label='cu Zxy')
    ax2.plot(vec, np.abs(Z[:, 1, 0].imag), '-c', label='cu Zyx')

    ax0.plot(vec, np.abs(Z[:, 0, 0].real), '-r', label='cu Zxx')
    ax0.plot(vec, np.abs(Z[:, 1, 1].real), '-m', label='cu Zyy')
    ax2.plot(vec, np.abs(Z[:, 0, 0].imag), '-r', label='cu Zxx')
    ax2.plot(vec, np.abs(Z[:, 1, 1].imag), '-m', label='cu Zyy')

    ax0.plot(vec, np.abs(ref[:, fi, 7]), ':^k', ms=2, label='ref')
    ax0.plot(vec, np.abs(ref[:, fi, 5]), ':^k', ms=2)
    ax2.plot(vec, np.abs(ref[:, fi, 8]), ':^k', ms=2, label='ref')
    ax2.plot(vec, np.abs(ref[:, fi, 6]), ':^k', ms=2)

    ax0.plot(vec, np.abs(ref[:, fi, 9]), ':^k', ms=2)
    ax0.plot(vec, np.abs(ref[:, fi, 3]), ':^k', ms=2)
    ax2.plot(vec, np.abs(ref[:, fi, 10]), ':^k', ms=2)
    ax2.plot(vec, np.abs(ref[:, fi, 4]), ':^k', ms=2)

    ax1.plot(vec, misfit1, '^:b', ms=2., lw=0.5, label='misfit Zxy')
    ax1.plot(vec, misfit2, '^:c', ms=2., lw=0.5, label='misfit Zyx')
    ax3.plot(vec, misfit3, '^:b', ms=2., lw=0.5, label='misfit Zxy')
    ax3.plot(vec, misfit4, '^:c', ms=2., lw=0.5, label='misfit Zyx')

    ax1.plot(vec, misfit5, '^:r', ms=2., lw=0.5, label='misfit Zxx')
    ax1.plot(vec, misfit6, '^:m', ms=2., lw=0.5, label='misfit Zyy')
    ax3.plot(vec, misfit7, '^:r', ms=2., lw=0.5, label='misfit Zxx')
    ax3.plot(vec, misfit8, '^:m', ms=2., lw=0.5, label='misfit Zyy')
else:
    for jj, ref_mod in enumerate(ref_mods):
        if lint == 0:
            line = 'line_line_y'
            ref = yref
            vec = np.linspace(-25, 25, 11)
        if lint == 1:
            line = 'line1_line_x'
            ref = xref1
            vec = np.linspace(-37.5, 37.5, 16)
        if lint == 2:
            line = 'line2_line_x'
            ref = xref2
            vec = np.linspace(-37.5, 37.5, 16)
        if lint == 3:
            line = 'line3_line_x'
            ref = xref3
            vec = np.linspace(-37.5, 37.5, 16)


        misfit1 = snrms(rhoa[:, 0, 1], ref[jj][:, fi, 5])
        misfit2 = snrms(rhoa[:, 1, 0], ref[jj][:, fi, 7])
        misfit3 = np.abs(phase[:, 0, 1] - ref[jj][:, fi, 6])
        misfit4 = np.abs(phase[:, 1, 0] - ref[jj][:, fi, 8])

        misfit5 = snrms(rhoa[:, 0, 0], ref[jj][:, fi, 3])
        misfit6 = snrms(rhoa[:, 1, 1], ref[jj][:, fi, 9])
        misfit7 = np.abs(phase[:, 0, 0] - ref[jj][:, fi, 4])
        misfit8 = np.abs(phase[:, 1, 1] - ref[jj][:, fi, 10])

        if jj == 0:
            ax0.plot(vec, rhoa[:, 0, 1], '-b')
            ax0.plot(vec, rhoa[:, 1, 0], '-c')
            ax2.plot(vec, phase[:, 0, 1], '-b', label='custEM xy')
            ax2.plot(vec, phase[:, 1, 0], '-c', label='custEM yx')

            ax0.plot(vec, rhoa[:, 0, 0], '-r')
            ax0.plot(vec, rhoa[:, 1, 1], '-m')
            ax2.plot(vec, phase[:, 0, 0], '-r', label='custEM xx')
            ax2.plot(vec, phase[:, 1, 1], '-m', label='custEM yy')

        ax0.plot(vec, ref[jj][:, fi, 7], style[jj], ms=3)
        ax0.plot(vec, ref[jj][:, fi, 5], style[jj], ms=3)
        ax2.plot(vec, ref[jj][:, fi, 8], style[jj], ms=3, label=ref_mods[jj])
        ax2.plot(vec, ref[jj][:, fi, 6], style[jj], ms=3)

        ax0.plot(vec, ref[jj][:, fi, 9], style[jj], ms=3)
        ax0.plot(vec, ref[jj][:, fi, 3], style[jj], ms=3)
        ax2.plot(vec, ref[jj][:, fi, 10], style[jj], ms=3)
        ax2.plot(vec, ref[jj][:, fi, 4], style[jj], ms=3)

        ax1.plot(vec, misfit1, style[jj][:-1] + 'b', ms=3., lw=0.5)
        ax1.plot(vec, misfit2, style[jj][:-1] + 'c', ms=3., lw=0.5)
        ax3.plot(vec, misfit3, style[jj][:-1] + 'b', ms=3., lw=0.5, label='custEM - ' + ref_mod)
        ax3.plot(vec, misfit4, style[jj][:-1] + 'c', ms=3., lw=0.5)

        ax1.plot(vec, misfit5, style[jj][:-1] + 'r', ms=3., lw=0.5)
        ax1.plot(vec, misfit6, style[jj][:-1] + 'm', ms=3., lw=0.5)
        ax3.plot(vec, misfit7, style[jj][:-1] + 'r', ms=3., lw=0.5)
        ax3.plot(vec, misfit8, style[jj][:-1] + 'm', ms=3., lw=0.5)

misfit1r = snrms(ref[0][:, fi, 5], ref[1][:, fi, 5])
misfit2r = snrms(ref[0][:, fi, 7], ref[1][:, fi, 7])
misfit3r = np.abs(ref[0][:, fi, 6] - ref[1][:, fi, 6])
misfit4r = np.abs(ref[0][:, fi, 8] - ref[1][:, fi, 8])

misfit5r = snrms(ref[0][:, fi, 3], ref[1][:, fi, 3])
misfit6r = snrms(ref[0][:, fi, 9], ref[1][:, fi, 9])
misfit7r = np.abs(ref[0][:, fi, 4] - ref[1][:, fi, 4])
misfit8r = np.abs(ref[0][:, fi, 10] - ref[1][:, fi, 10])

ax1.plot(vec, misfit1r, ':.b', ms=3., lw=0.5)
ax1.plot(vec, misfit2r, ':.c', ms=3., lw=0.5)
ax3.plot(vec, misfit3r, ':.b', ms=3., lw=0.5, label=ref_mods[0] + ' - ' + ref_mods[1])
ax3.plot(vec, misfit4r, ':.c', ms=3., lw=0.5)

ax1.plot(vec, misfit5r, ':.r', ms=3., lw=0.5)
ax1.plot(vec, misfit6r, ':.m', ms=3., lw=0.5)
ax3.plot(vec, misfit7r, ':.r', ms=3., lw=0.5)
ax3.plot(vec, misfit8r, ':.m', ms=3., lw=0.5)


# ax0.legend(loc=4, labelcolor='#002C72')
ax0.set_yscale('log')
ax0.set_xlabel('x (km)')

ax2.legend(labelcolor='#002C72')
ax2.set_xlabel('x (km)')
ax2.yaxis.tick_right()
ax2.yaxis.set_label_position("right")


if not ap:
    ax0.set_ylabel(r'|Z| real')
    ax2.set_ylabel(r'|Z| imag')
    ax2.set_yscale('log')
else:
    ax0.set_ylabel(r'|$\rho_a$|')
    ax2.set_ylabel(r'|$\phi$|')
    ylim = ax2.get_ylim()
    ax2.set_ylim(ylim[0] - 1, ylim[1] + 1)

# ax1.legend(labelcolor='#002C72')
ax1.set_yscale('log')
ax1.set_ylabel(r'|$\mathbf{\epsilon}$| (%)')
ax1.set_ylim([1e-2, 1e2])
ax1.set_xlabel('x (km)')

ax3.legend(labelcolor='#002C72')
ax3.set_xlabel('x (km)')
ax3.yaxis.tick_right()
ax3.yaxis.set_label_position("right")

fig.suptitle('0.0001 Hz, x-directed observation line', size=14, color='#002C72')

if not ap:
    ax3.set_yscale('log')
    ax3.set_ylabel(r'|$\mathbf{\epsilon}$| (%)')
    ax3.set_ylim([1e-2, 1e2])
else:
    ax3.set_ylim([0., 10.])
    ax3.set_ylabel(r'|$\Delta\phi$| (°)')
if not ap:
    plt.savefig('plots/' + ref_mod[:-3] + '_f_' + str(freqs[fi]) + '_' + line +
                '_ri.pdf', bbox_inches='tight')
else:
    plt.savefig('plots/' + ref_mod[:-3] + '_f_' + str(freqs[fi]) + '_' + line +
                '_ap.pdf', bbox_inches='tight')
plt.show()


