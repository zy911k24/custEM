# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import numpy as np
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld


n_rx = 16
# define and refine rx points
vec = np.linspace(-37.5e3, 37.5e3, n_rx)
rx = np.zeros((3 * n_rx + 11, 3))

rx[:16, 0] = vec
rx[16:32, 0] = vec
rx[32:48, 0] = vec

rx[:16, 1] = -15e3
rx[32:48, 1] = 15e3
rx[-11:, 1] = np.linspace(-25e3, 25e3, 11)


# test different Rx depths below surface to avoid low-frequency artifacts
rx[:, 2] = -20.

rx_tri = mu.refine_rx(rx, 10., 30.)

# Note, for meshing purposes, the blocks are devided into sub_blocks with
# identical sub-surfaces to simplify writing the TetGen input file

# block 1
x1 = [-2.5e3, 2.5e3]
y1 = [[-20e3, -15e3], [-15e3, 0.], [0., 15e3], [15e3, 20e3]]
z1 = [-20e3, -5e3]

# block 2
x2 = [[-2.5e3, 2.5e3], [2.5e3, 22.5e3]]
y2 = [-15e3, 0.]
z2 = [-25e3, -20e3]

# block 3
x3 = [[-22.5e3, -2.5e3], [-22.5e3, -2.5e3], [-2.5e3, 2.5e3], [-2.5e3, 2.5e3]]
y3 = [0., 15e3]
z3 = [[-50e3, -25e3], [-25e3, -20e3], [-50e3, -25e3], [-25e3, -20e3]]

block_cell_size = 1e9

dim = 1e8
M = BlankWorld(name='dublin',
               x_dim=[-dim, dim],
               y_dim=[-dim, dim],
               z_dim=[-dim, dim],
               m_dir='./meshes',
               preserve_edges=True,
               )

M.build_surface() #insert_paths=rx_tri)
M.build_halfspace_mesh()

# add first block
for ix in range(len(y1)):
    M.add_brick(start=[x1[0], y1[ix][0], z1[0]],
                stop=[x1[1], y1[ix][1], z1[1]],
                cell_size=block_cell_size,
                marker=2)

# add second block
for ix in range(len(x2)):
    M.add_brick(start=[x2[ix][0], y2[0], z2[0]],
                stop=[x2[ix][1], y2[1], z2[1]],
                cell_size=block_cell_size,
                marker=3)

# add third block
for ix in range(len(z3)):
    M.add_brick(start=[x3[ix][0], y3[0], z3[ix][0]],
                stop=[x3[ix][1], y3[1], z3[ix][1]],
                cell_size=block_cell_size,
                marker=4)


M.add_rx(rx[:16])
M.add_rx(rx[16:32])
M.add_rx(rx[32:48])
M.add_rx(rx[-11:])

M.add_paths(rx_tri)

#M.extend_world(1e3, 1e3, 1e3)
M.remove_duplicate_poly_faces()
M.call_tetgen(tet_param='-pq1.2aAT1e-10', export_vtk=True)


