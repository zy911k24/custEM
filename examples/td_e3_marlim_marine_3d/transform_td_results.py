# -*- coding: utf-8 -*-
"""
Created on Tue Dec 09 16:06:00 2014

@author: moouuzi
"""


import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams as rc
from scipy.integrate import cumtrapz
from scipy.interpolate import interp1d
from pyfftlog import fhti, fftl
from scipy.interpolate import InterpolatedUnivariateSpline as iuSpline
import empymod


def resample(t_in, field_in, t_out):

    func = interp1d(t_in, field_in, kind='cubic')
    return(func(t_out))


# load optimized time points for pyfftlog algorithm and specify frequencies
ref_freq = [0.125, 0.25, 0.5, 0.75, 1., 1.25]
pts_rec2 = np.load('pts_req_80_-4_2.npy')
pts_out2 = np.load('pts_out_80_-4_2.npy')

# %% plot B

path = './results/E_RA/e3_marlim_results/'
symb = ['^:', 's-.']

name = 'p2'

B_field0, B_field1, B_field2 = [], [], []
fs = 12
stations = range(204)
fem_times = np.logspace(-2, 2, 321)
c_str = ['x', 'y', 'z']

for line in ['inline', 'broadside']:
    all_export = []
    for cmp in [0, 1, 2]:
        print('...  transforming ' + c_str[cmp] + '-component for all stations'
              ' on ' + line + ' observation line  ...')
        export = []
        for kk, stn in enumerate(stations):
            ra_result = []
            for jj in range(len(fem_times)):
                tmp = np.load(path + name + '_interpolated/E_shut_on_' +
                              name + '_on_' + line + '_path_line_x_' +
                              str(jj) + '.npy')
                ra_result.append(tmp[::-1][stn, :3])

            # transofrm to numpy array and compensate dipole moment
            ra_result = np.array(ra_result)[:, cmp] / 10.
            copy = np.zeros(ra_result.shape)
            copy[:] = ra_result[:]

            # replace noisy signal before signal onset and extrapolate the
            # signal to the range -4
            for jj in range(len(ra_result)):
                try:
                    # increasing amplitude (shut-on signal) criterium
                    if np.abs(ra_result[-jj-1]) > np.abs(ra_result[-jj-2]):
                        pass
                    else:
                        # amplitude threshold criterium
                        if np.abs(ra_result[-jj-1]) > np.abs(
                                ra_result[-1]) / 1e4:
                            pass
                        else:
                            # idx of signal onset reached
                            break
                except:
                    break

            # overwrite original signal
            ra_result[:-jj] = ra_result[-jj]

            # spline interpolate on fftlog time range
            b_spline = iuSpline(np.log(fem_times),
                                np.log10(np.abs(ra_result)), k=1)
            to_trans = 10**b_spline(np.log(pts_rec2))

            # ############################################################### #
            #         pyFFTLog algorithm, big thanks @ D. Werthmüller
            # ############################################################### #

            # FFTLog parameters
            pts_per_dec = 80    # time point per decade
            add_dec = [-4, 2]   # add decades on each side
            q = 1.              # +1 leades to best results

            # Calculate minimum and maximum required inputs
            ftpts = np.logspace(-4., 2., pts_per_dec)
            rmin = np.log10(1/ftpts.max()) + add_dec[0]
            rmax = np.log10(1/ftpts.min()) + add_dec[1]
            n = np.int(rmax - rmin)*pts_per_dec

            # Pre-allocate output
            f_resp = np.zeros(np.array(ref_freq).shape, dtype=complex)

            for mu in [0.5, -0.5]:

                # Central point log10(r_c) of periodic interval
                logrc = (rmin + rmax)/2

                # Central index (1/2 integral if n is even)
                nc = (n + 1)/2.

                # Log spacing of points
                dlogr = (rmax - rmin)/n
                dlnr = dlogr*np.log(10.)

                # Calculate required input x-values

                # Initialize FFTLog
                kr, xsave = fhti(n, mu, dlnr, q, kr=1, kropt=1)

                # Calculate pts_out with adjusted kr
                logkc = np.log10(kr) - logrc

                # rk = r_c/k_r; adjust for Fourier transform scaling
                rk = 10**(logrc - logkc)*np.pi/2

                # Calculate required times/frequencies with the dipole solution
                t2f_t_resp = to_trans

                # Carry out FFTLog
                t2f_f_coarse = fftl(t2f_t_resp, xsave.copy(), rk, 1)

                # Interpolate for required frequencies/times
                t2f_f_spline = iuSpline(np.log(pts_out2), t2f_f_coarse)

                # conduct transformation for signal == 1 (shut-on)
                if mu > 0:
                    f_resp += t2f_f_spline(
                        np.log(ref_freq))*np.array(ref_freq)
                else:
                    f_resp += 1j*t2f_f_spline(
                        np.log(ref_freq))*np.array(ref_freq)

            export.append(f_resp)
        all_export.append(export)

    np.save('data/' + line + '.npy', np.array(all_export))
