﻿################################################################################
# # # # #                    time-domain Example 3                     # # # # #
################################################################################

      comparison of RA time-domain results with Marlim R3D reference model 

################################################################################

                               provide input data

    - Download the resistivity data, topography information, and survey data 
      without noise from https://zenodo.org/record/400233. The following files
	  are required to be stored in the "data" directory:
	  
	    Sea_Bottom-mr3d.xyz
        Miocene-mr3d.xyz
        Oligocene-mr3d.xyz
        Blue_mark-mr3d.xyz
        Top_of_Salt-mr3d.xyz
        Base_of_salt-mr3d.xyz
    
        Horizontal_resistivity.sgy
        Vertical_Resistivity.sgy
		
	- The reference data of the inline and broadside observation lines are
	  provided in the *marlim_survey.nc* file (Big Thanks @ D. Werthmüller).

	- Install the required dependencies for generating the input data. Via
	  conda, it will be (within your custEM environment):
	  
	    > conda install -c conda-forge empymod, xarray, discrtize, segyio
   
    - Run the *modify_marlim_input_data.py* script:
	
	    > python modify_marlim_input_data.py
	
################################################################################

    - Start with generating the mesh by running the *mesh_td_example_3.py* file:

        > python mesh_td_example_3.py
    
    - The *run_td_example_3.py* script should be called in parallel:

        > mpirun -n 16 python -u run_example_1.py (and export OMP_NUM_THERADS=4,
                                                   recommended for p2)
    
        The computation will requie ~280 GB RAM with this 
        parallelization configuration.  
		
	- Transform the RA solutions to time domain with help of the *pyfftlog.py*
	  algorithm by D. Werthmüller (https://github.com/prisae/pyfftlog). A 
	  version of the corresponding *pyfttlog.py* script (version from
	  30.06.2020) is attached to this repository since the license allow it.
	  Run the *transform_td_results.py* script to run the transformation:
	  
	    > python transform_td_results.py
    
    - The *plot_td_example_3.py* and *plot_td_fd_comparison.py* scripts
	  will generate identical plots as shown in the corresponding publication
	  (Rochlitz et al., 2021) in the *plots* directory.

        > python plot_td_example_3.py 
        > python plot_td_fd_comparison.py 
		
################################################################################