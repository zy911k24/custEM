########
Examples
########

The following examples (**examples** directory in the repository) are useful
to learn about the usage of different features of custEM. Furthermore, they
can be used to reproduce the presented results or to benchmark 
individual solutions.

Please note that the handling / code syntax / solutions can differ between 
various custEM versions and lead to incompatibility issues, if the example
files version does not match the custEM version.

Anyway, the custEM version 1.0 contains the time-domain and frequency-domain 
modeling benchmark examples. Additional (unpublished) examples will be added 
soon to present the handling of advanced modeling problems. In custEM
version 1.0, all examples were tested for their functionality using the most
recent code syntax.

################################################################################

Frequency Domain (Rochlitz et. al., 2019)
#########################################

The results of the four frequencdy-domain modeling examples, presented by
Rochlitz et. al. (2019), can be reproduced by executing the provided python
files in the corresponding directories, denoted *fdom_eN*, *N*=1-4.
Further information is provided in the README files of each subdirectory.

################################################################################

Time Domain (Rochlitz et. al., 2021)
####################################

The results of the three time-domain modeling examples, presented by
Rochlitz et. al. (2021), can be reproduced by executing the provided python
files in the corresponding directories. denoted *tdom_eN*, *N*=1-3,
Further information is provided in the README files of each subdirectory.

################################################################################

Further examples
################

These examples will be added soon with a proper description. The results were
already computed, but providing a proper description and maintainance of
additional examples requires some effort. Interested users can contact 
raphael.rochlitz@leibniz-liag.de for any information regarding the new examples.

The following examples are so far not inlcuded in any peer-reviewed publication.
However, they contain helpful input about handling and validating advanced EM
modeling topics such as topography, ip effects, full anisotropy and others.

Marine CSEM model with general anisotropy
-----------------------------------------

- work in progress, will be fd_e5

1D validation of frequency-domain IP modeling
---------------------------------------------

- added as fd_e6

MT validation of 3D Dublin test model 
-------------------------------------

- added as mt_e1

