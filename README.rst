
.. sphinx-inclusion-marker


!!! custEM 1.0 supporting CSEM, TEM, and MT modeling out now !!!
 
The Python toolbox **custEM** is an open-source development for customizable
3D finite-element modeling of controlled-source, transient, and
natural-source electromagnetic data. The toolbox is based on 
the finite-element library FEniCS, see https://fenicsproject.org/ . Different 
total and secondary electric or magnetic field as well as gauged-potential
approaches are implemented. In addition, **custEM** contains a mesh generation
submodule for the straightforward generation of tetrahedral meshes supporting
a large number of marine, land-based, airborne or mixed model scenarios.
Interpolation and visualization tools simplify the post-processing workflow.

Mesh generation runs on any laptop or desktop PC. Also minimalistic examples
can be tested on such machines. Because of using direct solvers, we recommend
to get access to a cluster with >= 32 cores/threads and >=256 GB RAM for
calculating sufficiently accurate models for more complex 3D setups. However,
quite good results for simpler 3D modeling studies often require only 32-64 GB.

Please note that custEM is under continuous development. For information about 
planned updates and previous development steps we refer to the 
:ref:`Development <devlabel>` section. Do not hesitate to contact
raphael.rochlitz@leibniz-liag.de for any kind of question about custEM or
discovered issues in the code, examples, or the documentation.

################################################################################

Getting started
===============

To guide you through setting up the toolbox and making third party dependencies
available, please follow the :ref:`Installation <installlabel>` notes.
After A successful installation,
user could run any of the test files provided in the corresponding directory.
In addition to these initial tests for the overall functionality of **custEM**,
we refer to the provided examples. The first four frequency-domain and first 
three time-domain examples correspond to the ones presented by
(Rochlitz et al., 2019) & (Rochlitz et al., 2021), respectively.
Further advise to develop solution strategies for user-specific modeling
problems is provided by the tutorials.

In addition, we recommend to read the :ref:`Tips&Tricks <adviselabel>` section,
which contains further practical advise based on our modeling experiences.

################################################################################

Installation
============

For instructions, it is referred to the :ref:`Installation <installlabel>`
section.

################################################################################

Documentation
=============

The python API documentation is available online on ReadtheDocs. In, addition,
the complete API documentation is available as "hmtl_doc" zip-file in
the *docs* directory of the custEM repository and can be accessed via opening
the "index.html" file in the zip directory with your favorite browser.

The :ref:`Source code <moduleslabel>` documentation content is generated
automatically when publishing new custEM versions. Detailed information
about specific functionalities might be accessed with help of the
:ref:`genindex` or using the :ref:`search`.

.. figure:: custEMoverview.png
  :align: center
  :scale: 30 %
  
  Overview of Third-party dependencies and modules in custEM

################################################################################

Tutorials
=========

Tutorials are available in the *tutorials* directory in the **custEM**
repository as *jupyter notebook*, *python* script, and in *html* format.
The *html* documentation of the :ref:`Tutorials <tutoriallabel>` can be
also accessed in the corresponding section. Please note that the usage of
*jupyter notebooks* is not straightforward in combination with multiprocessing.
For running the *run_tutorial*, please use the provided *run_tutorial.py*
script and call it from the command line with the *mpirun* syntax:

    --> mpirun -n x python run_tutorial.py

with x, the number of MPI processes.

################################################################################

Examples
========

A selection of CSEM modeling examples can be found :ref:`here <examplelabel>`.

################################################################################

License
=======

Copyright 2016-2020 by R. Rochlitz

The **custEM** toolbox is licensed under the GNU GENERAL PUBLIC LICENSE
(GPL). The terms are listed here :ref:`License <licenselabel>`.

################################################################################

Authors
=======

Contributing authors and contacts are listed in the
:ref:`Authors <authorlabel>` section.

################################################################################

References
==========

Rochlitz, R., Skibbe, N. and Günther, T. (2019), 
*custEM: customizable finite element simulation of complex controlled-source
electromagnetic data*,
GEOPHYSICS Software and Algorithms.

Rochlitz, R. (2020), 
*Analysis and open-source Implementation of
Finite Element Modeling techniques for
Controlled-Source Electromagnetics*,
PhD thesis, Westfälische Wilhelms-Universität Münster, Germany.

Rochlitz, R., Seidel, M. and Börner, R.-U. (2021), 
*Evaluation of three approaches for simulating time-domain
electromagnetic data using the open-source software custEM*,
Geophysical Journal International, accepted manuscript, doi: 10.1093/gji/ggab302

Werthmüller, D., Rochlitz, R., Castillo-Reyes, O., and Heagy, L. (2021),
*Towards an open-source landscape for 3-D CSEM modelling*,
Geophysical Journal International, 227(1), 644-659.

If required, please contact raphael.rochlitz@leibniz-liag.de for access.

Next to us, don't forget to give credits to the authors of FEniCS, pyGIMLi, 
TetGen and COMET if submodules based on their developments are used.
