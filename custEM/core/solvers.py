# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import time
from custEM.misc import logger_print as lp
from custEM.misc import current_mem
from petsc4py import PETSc


class Solver:

    """
    Solver class called internally from MOD instance.

    Methods
    -------

    - solve_var_form_default()
        solve variaitonal formulation using default FEniCS solver,
        no support of symmetry, memory limits

    - solve_system_default()
        solve assembled system of equations with default FEniCS solver,
        no support of symmetry, memory limits

    - solve_system_mumps()
        use MUMPS solver to solve system of equations,
        no memory limits and symmetry support

    - call_mumps()
        call MUMPS solver within *solve_system_mumps()* method

    - solve_var_form_iter()
        use iteative solvers to solve variational formulation, in development

    - solve_system_iter()
        use iteative solvers to solve system of equations, in development

    BoundaryConditions
    ------------------

    - default choice of boundary conditions are implicit Neumann BC

    - for zero Dirichlet BC, change *bc* argument in the 'solve' functions
      or during assembly for all total field approaches
    """

    def __init__(self, FS, FE, mumps_debug=False, serial_ordering=False):

        """
        Initializes Solver instance for assembling and solution of FE systems.

        Required arguments
        ------------------

        - FS, type class
            FunctionSpaces instance

        - FE, type class
            FE-approach (e.g., **E_vector**) instance

        Keyword arguments
        -----------------

        - mumps_debug = False, type bool
            set **True** during **MOD** class initialization for enabling
            debugging MUMPS by adjusting the MUMPS internal log level

        - serial_ordering = False, type bool
            use serial ordering of *METIS* instead parallel ordering of
            *SCOTCH*; leads to slight increase of solution time for *MUMPS*
            but significantly reduces chance of potential *MUMPS* crashes
        """

        self.FS = FS
        self.FE = FE

        #df.PETScOptions.set("mat_mumps_icntl_13", 1)
        df.PETScOptions.set("mat_mumps_icntl_10", 0)
        if serial_ordering:
            df.PETScOptions.set("mat_mumps_icntl_28", 1)
        else:
            df.PETScOptions.set("mat_mumps_icntl_28", 2)
        df.PETScOptions.set("mat_mumps_cntl_1", 1e-8)
        # df.PETScOptions.set("mat_mumps_cntl_4", 0.)
        # df.PETScOptions.set("mat_mumps_cntl_7", 1e-10)
        # df.PETScOptions.set("mat_mumps_icntl_35", 2)  # BLR test
        # choose SCOTCH explicitly, not necessary as icntl 28 forces SCOTCH
        # df.PETScOptions.set("mat_mumps_icntl_29", 1)
        if mumps_debug:
            df.PETScOptions.set("mat_mumps_icntl_4", 3)

    def solve_var_form_default(self, L, R, U, bc=None):

        """
        Use default petsc_lu solver to solve a LinearVariationProblem.

        Required arguments
        ------------------

        - L = left-hand-side, type UFL form
            not assembled! UFL form of the left-hand-side of the system of
            equations

        - R = right-hand-side, type UFL form
            not assembled! UFL form of the right-hand-side of the system of
            equations

        - U = Solution space, type dolfin FunctionSpace
            mixed solution FunctionSpace

        Keyword arguments
        -----------------

        - bc = None, type str
            so far *ZeroDirichlet* or short *ZD* bc
            and implicit Neumann bc by using *None* are supported
        """

        t_0 = time.time()

        lp(self.FE.MP.logger, 20,
           '  -  default petsc LU solver initialized  -  ')
        lp(self.FE.MP.logger, 20, '...  solving system  ...', pre_dash=False)
        if bc is None:
            df.solve(L == R, U)

        elif bc == 'ZeroDirichlet' or bc == 'ZD':
            self.FS.add_dirichlet_bc()
            df.solve(L == R, U, bcs=self.FS.bc)

        self.FS.solution_time = time.time() - t_0
        lp(self.FE.MP.logger, 20,
           '  -  solution time for FE system including assembly (s)  -->  ' +
           str(self.FS.solution_time) + '  -  ', pre_dash=False)

    def solve_system_default(self, A, b):

        """
        Use default petsc_lu solver to solve an assembled system of equations.

        Required arguments
        ------------------

        - A = left-hand-side matrix, type PETScMatrix
            assembled LHS matrix

        - b = right-hand-side vector, type PETScVector
            assembled RHS vector
        """

        t_0 = time.time()

        lp(self.FE.MP.logger, 20,
           '  -  default petsc LU solver initialized  -  ')
        lp(self.FE.MP.logger, 20, '...  solving system  ...', pre_dash=False)

        df.solve(A, self.FS.U.vector(), b.vector())

        self.FS.solution_time = time.time() - t_0
        lp(self.FE.MP.logger, 20,
           '  -  solution time for main system of equations (s)  -->  ' +
           str(self.FS.solution_time) + '  -  ', pre_dash=False)

    def solve_system_mumps(self, A, b, fs=None, sym=False, out_of_core=False,
                           first_call=True, tx_selection=None):

        """
        Use MUMPS solver to solve an assembled linear system of equations.

        Required arguments
        ------------------

        - A = left-hand-side matrix, type PETScMatrix
            assembled LHS matrix

        - b = right-hand-side vector, type PETScVector
            assembled RHS vector

        Keyword arguments
        -----------------

        - fs = None, type str or dolfin FunctionSpace
            define FunctionSpace for solution function(s)

        - sym = False, type int or bool
            set to **1** for positive definite symmetric matrix
            set to **2** or **True** for general structurally symmetric matrix

        - out_of_core = False, type bool
            set to **True** if MUMPS should be allowed to use disc space for
            the solution process if the memory is completely exploited

        - tx_selection = None, type list of int
            specify if system should be solved for not all but only selected
            right-hand sides; so far used to solve DC system only for grounded
            Tx if there are also ungrounded ones in the same model

        - first_call = True, type bool
            specify if this is the first call of a solution based on the same
            system matrix, re-use factorization if first_call=**False**
        """

        print_dummy = False
        if not first_call:
            print_dummy = True

        if fs is None:
            self.U = [df.Function(self.FS.M) for ti in range(self.FE.n_tx)]
        elif fs == 'scalar':
            self.U = [df.Function(self.FS.S) for ti in range(self.FE.n_tx)]
        else:
            self.U = [df.Function(fs) for ti in range(self.FE.n_tx)]

        counter = -1
        second_call = True

        for ti in range(self.FE.n_tx):
            if not first_call:
                if second_call:
                    lp(self.FE.MP.logger, 20,
                       '...  solving additional right-hand sides  ...',
                       pre_dash=False)
                    t2 = time.time()
                second_call = False
            try:
                to_solve = b[ti].vector()
            except AttributeError:
                to_solve = b[ti]

            if tx_selection is None:
                if df.MPI.rank(df.MPI.comm_world) == 0:
                    self.U[ti].vector()[:]
                self.call_mumps(A, to_solve, self.U[ti].vector(), sym,
                                out_of_core, first_call)
                first_call = False
            else:
                if tx_selection[ti]:
                    self.call_mumps(A, to_solve, self.U[ti].vector(), sym,
                                    out_of_core, first_call)
                first_call = False
            counter += 1
            if counter > 0 and counter % 10 == 0:
                lp(self.FE.MP.logger, 20,
                   '  -  solved right-hand '
                   'sides counter  -->  ' + str(counter) + '  -  ')

        if self.FE.n_tx > 1:
            if print_dummy:
                lp(self.FE.MP.logger, 20,
                   '  -  solution time for ' + str(self.FE.n_tx) +
                   ' additional right-hand sides (s)  -->  ' +
                   str(int(time.time() - t2)) + '  -  ', pre_dash=False)
            else:
                lp(self.FE.MP.logger, 20,
                   '  -  solution time for ' + str(self.FE.n_tx - 1) +
                   ' additional right-hand sides (s)  -->  ' +
                   str(int(time.time() - t2)) + '  -  ', pre_dash=False)

        if fs is None:
            self.FS.U = self.U
        else:
            return(self.U)

    def call_mumps(self, A, b, u, sym, out_of_core, first_call=True):

        """
        Call MUMPS solver.

        Required arguments
        ------------------

        - A = left-hand-side matrix, type PETScMatrix
            assembled LHS matrix

        - b = right-hand-side vector, type PETScVector
            assembled RHS vector

        - u = solution vector, type PETScVector
            empty solution vector to be filled

        - see the definitions of the *solve_system_mumps()* method for the
          description of the other input parameters
        """

        t_0 = time.time()
        if first_call:
            if out_of_core:
                df.PETScOptions.set("mat_mumps_icntl_22", 1)
                lp(self.FE.MP.logger, 30,
                   'Warning! OUT_OF_CORE option applied which can makes the '
                   'solution process very slow if the available RAM is '
                   'exceeded. Continuing  ...')

            if type(sym) is bool and sym:
                A.mat().setOption(PETSc.Mat.Option.SYMMETRIC, True)
                lp(self.FE.MP.logger, 20,
                   '  -  general symmetric MUMPS solver initialized  -  ',
                   pre_dash=False)
            elif sym == 1:
                A.mat().setOption(PETSc.Mat.Option.SPD, True)
                lp(self.FE.MP.logger, 20,
                   '  -  positive definite symmetric MUMPS solver '
                   'initialized  -  ', pre_dash=False)
            elif sym == 2:
                A.mat().setOption(PETSc.Mat.Option.SYMMETRIC, True)
                lp(self.FE.MP.logger, 20,
                   '  -  general symmetric MUMPS solver initialized  -  ',
                   pre_dash=False)
            else:
                A.mat().setOption(PETSc.Mat.Option.SYMMETRIC, False)
                A.mat().setOption(PETSc.Mat.Option.SPD, False)
                lp(self.FE.MP.logger, 20,
                   '  -  unsymmetric MUMPS LU solver initialized  -  ',
                   pre_dash=False)

            self.solver = df.PETScLUSolver(self.FS.mesh.mpi_comm(), A, "mumps")
            lp(self.FE.MP.logger, 20,
               '...  solving system  ...', pre_dash=False)
            self.A = A

        self.solver.solve(u, b)
        self.FS.solution_time += time.time() - t_0

        if first_call:
            lp(self.FE.MP.logger, 20,
               '  -  accumulated solution time for FE system (s)  -->  ' +
                    str(int(self.FS.solution_time)) + '  -  ', pre_dash=False)

    def solve_var_form_iter(self,  L, R, U, bc, sym=False,
                            method='gmres', pc='petsc_amg',
                            abs_tol=1e-6, rel_tol=1e-6, maxiter=2000):

        """
        Use Krylov solver to solve a LinearVariationProblem.

        Required arguments
        ------------------

        - L = left-hand-side, type UFL form
            not assembled! UFL form of the left-hand-side of the system of
            equations

        - R = right-hand-side, type UFL form
            not assembled! UFL form of the right-hand-side of the system of
            equations

        - U = Solution space, type dolfin FunctionSpace
            mixed solution FunctionSpace

        - bc = boundary conditions, type str
            so far **'ZeroDirichlet'** or short **'ZD'** bc
            and implicit Neumann bc by using **None** are supported

        Keyword arguments
        -----------------

        - sym = False, type bool
            set to **True** if a symmetric system of equations is solved

        - method = 'gmres', type str
            iterative solution method

        - pc = 'petsc_amg', type str
            prconditioning type

        - abs_tol = 1e-6, type float
            absolute tolereance

        - rel_tol = 1e-6, type float
            relative tolerance

        - maxiter = 2000, type int
            maximum number of iterations
        """

        start = time.time()

        lp(self.FE.MP.logger, 50,
           'Error! Using Krylov solver does not work proberly yet. '
           'Aborting  ...')
        raise SystemExit

        if bc is None:
            problem = df.LinearVariationalProblem(L, R, U)

        elif bc == 'ZeroDirichlet' or bc == 'ZD':
            self.FS.add_dirichlet_bc()
            problem = df.LinearVariationalProblem(L, R, U, self.FS.bc)

        solver = df.LinearVariationalSolver(problem)
        # solver.parameters['symmetric'] = sym
        solver.parameters['linear_solver'] = method
        solver.parameters['preconditioner'] = pc
        solver.parameters['krylov_solver']['monitor_convergence'] = True
        solver.parameters['krylov_solver']["absolute_tolerance"] = abs_tol
        solver.parameters['krylov_solver']["relative_tolerance"] = rel_tol
        solver.parameters['krylov_solver']["maximum_iterations"] = maxiter
        # solver.parameters['krylov_solver']['nonzero_initial_guess'] = True
        lp(self.FE.MP.logger, 20, '...  solving system  ...', pre_dash=False)
        solver.solve()

        stop = time.time() - start
        lp(self.FE.MP.logger, 20,
           '  -  solution time for FE system including assembly (s)  -->  ' +
           str(self.FS.solution_time) + '  -  ', pre_dash=False)

    def solve_system_iter(self, A, b, sym=False,
                          method='gmres', pc='petsc_amg',
                          abs_tol=1e-6, rel_tol=1e-6, maxiter=2000):

        """
        Use Krylov solver to solve an assembled linear system of equations.

        Required arguments
        ------------------

        - A = left-hand-side matrix, type PETScMatrix
            assembled LHS matrix

        - b = right-hand-side vector, type PETScVector
            assembled RHS vector

        Keyword arguments
        -----------------

        - sym = True, type bool
            set to **False** if an unsymmetric system of equations is solved

        - method = 'gmres', type str
            iterative solution method

        - pc = 'petsc_amg', type str
            prconditioning type

        - abs_tol = 1e-6, type float
            absolute tolereance

        - rel_tol = 1e-6, type float
            relative tolerance

        - maxiter = 2000, type int
            maximum number of iterations
        """

        lp(self.FE.MP.logger, 50,
           'Error! Using Krylov solver does not work proberly yet. '
           'Aborting  ...')
        raise SystemExit

        t_0 = time.time()

        # prm = df.parameters.krylov_solver  # short form
        # prm.absolute_tolerance = abs_tol
        # prm.relative_tolerance = rel_tol
        # prm.maximum_iterations = maxiter
        # prm.monitor_convergence = True

        # set PETSc solve type (conjugate gradient) and preconditioner
        # (algebraic multigrid)
        df.PETScOptions().set("ksp_type", method)
        df.PETScOptions().set("pc_type", pc)
        # df.PETScOptions.set("ksp_knoll")
        df.PETScOptions().set("pc_hypre_type", "boomeramg")
        # df.PETScOptions().set("pc_hypre_boomeramg_agg_nl", 10)
        # df.PETScOptions().set("pc_hypre_boomeramg_max_levels", 500)
        # df.PETScOptions().set("pc_hypre_boomeramg_relax_weight_all", 0.7)

        # df.PETScOptions.set("pc_hypre_euclid_levels", 3)
        # df.PETScOptions().set("ksp_gmres_restart", 50)
        # df.PETScOptions().set("ksp_monitor_true_residual")
        df.PETScOptions().set("ksp_max_it", maxiter)
        df.PETScOptions().set("ksp_rtol", rel_tol)
        df.PETScOptions().set("ksp_atol", abs_tol)
        # # since we have a singular problem, use SVD solver on the multigrid
        # # 'coarse grid'
        # PETScOptions.set("mg_coarse_ksp_type", "preonly")
        # PETScOptions.set("mg_coarse_pc_type", "svd")
        #
        # # set the solver tolerance
        # PETScOptions.set("ksp_rtol", 1.0e-8)
        #
        # # print PETSc solver configuration
        # df.PETScOptions.set("ksp_view")
        df.PETScOptions.set("ksp_monitor")

        # Create Krylov solver and set operator
        solver = df.PETScKrylovSolver()
        solver.set_operator(A)

        # Set PETSc options on the solver
        solver.set_from_options()
        lp(self.FE.MP.logger, 20, '...  solving system  ...', pre_dash=False)
        # Solve
        solver.solve(self.FS.U.vector(), b.vector())

        # solver = df.KrylovSolver(method, pc)
        # solver.solve(A, self.FS.U.vector(), b.vector())

        stop = time.time() - t_0
        lp(self.FE.MP.logger, 20,
           '  -  solution time for main system of equations  -->  ' +
           str(stop) + '  -  ', post_dash=True)
