# -*- coding: utf-8 -*-
"""
core
====

Submodules:

- **model_base** for defining main model class
- **pre_proc** for defining pre-processing class 
- **post_proc_fd** for defining frequency-domain post-processing class
- **post_proc_td** for defining time-domain post-processing class
- **solver** for solving linear systems of equations

################################################################################
"""
from . model_base import *
from . solvers import *
from . post_proc_td import *
from . post_proc_fd import *
from . pre_proc import *

# THE END
