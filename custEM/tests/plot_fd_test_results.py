# -*- coding: utf-8 -*-
"""
Make some plots stored in the 'fd_test_plots' directory.

Note: 1./2. result, 3./4. as well as 5./6. should be similar since the same
      model was computed by two approaches each
"""


import os
from custEM.post import PlotFD as Plot
import matplotlib.pyplot as plt


plt.ioff()   # speed things up
E_flg = '_E_t'
H_flg = '_H_t'
EH_flg = 'H'    # change to E for E-field plots
pp1 = '1'
pp2 = '2'
fs = 10
Elim = [1e-9, 1e-3]
Hlim = [1e-6, 1e-2]
errlim = [1e-1, 1e1]
comps = ['x', 'y', 'z', 'mag']

mod0 = 'fd_test_E_t'
mod1 = 'fd_test_E_s_pf_E'
mod2 = 'fd_test_E_s_pf_H'
mod3 = 'fd_test_Am_t'
mod4 = 'fd_test_An_t'
mod5 = 'fd_test_Am_s'
mod6 = 'fd_test_Fm_s'
mod7 = 'fd_test_H_s_pf_E'
mod8 = 'fd_test_H_s_pf_H'
mod9 = 'fd_test_An_s'


mesh1 = 'hed_halfspace_mesh'
mesh2 = 'line_dip_plate_mesh'
mesh3 = 'loop_brick_mesh'

line1 = 'default_small_line_x'
line2 = 'default_small_line_y'
line3 = 'default_small_line_z'
line4 = 'default_large_line_x'
line5 = 'default_large_line_y'
line6 = 'default_large_line_z'
lines = [line1, line2, line3, line4, line5, line6]

slice1 = 'default_small_slice_x'
slice2 = 'default_small_slice_y'
slice3 = 'default_small_slice_z'
slice4 = 'default_large_slice_x'
slice5 = 'default_large_slice_y'
slice6 = 'default_large_slice_z'
slices = [slice1, slice2, slice3, slice4, slice5, slice6]

lims = [2., 2., 2., 10., 10., 10.]

###############################################################################
# # #                      make default line plots                        # # #
###############################################################################

if not os.path.isdir('fd_test_plots/line_plots'):
    os.makedirs('fd_test_plots/line_plots')
if not os.path.isdir('fd_test_plots/slice_plots'):
    os.makedirs('fd_test_plots/slice_plots')

P1 = Plot(mod=mod0, mesh=mesh1, approach='E_t', fig_size=[8, 12],
          r_dir='fd_test_results', s_dir='fd_test_plots/line_plots')

for k in ['small', 'large']:
    P1.load_default_line_data(interp_meshes=k)
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod1, sf=False, approach='E_s')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod2, sf=False, approach='E_s')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod3, mesh=mesh2, approach='Am_t')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod4, mesh=mesh2, approach='An_t')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod5, mesh=mesh2, sf=False, approach='Am_s')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod6, mesh=mesh2, sf=False, approach='Fm_s')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod7, mesh=mesh3, sf=True, approach='H_s')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod8, mesh=mesh3, sf=True, approach='H_s')
    P1.load_default_line_data(interp_meshes=k,
                              mod=mod9, mesh=mesh3, sf=True, approach='An_s')

for i, line in enumerate(lines):
    print('plotting field data: ', line)
    P1.plot_line_data(mod=mod0, mesh=line, xlim=[-lims[i], lims[i]],
                      save=False)
    P1.plot_line_data(mod=mod1, mesh=line, xlim=[-lims[i], lims[i]],
                      new=False, save=False)
    P1.plot_line_data(mod=mod2, mesh=line, xlim=[-lims[i], lims[i]],
                      new=False, save=False)
    P1.plot_line_data(mod=mod3, mesh=line, xlim=[-lims[i], lims[i]],
                      new=False, save=False)
    P1.plot_line_data(mod=mod4, mesh=line, xlim=[-lims[i], lims[i]],
                      new=False, save=False)
    P1.plot_line_data(mod=mod5, mesh=line, xlim=[-lims[i], lims[i]],
                      new=False, save=False)
    P1.plot_line_data(mod=mod6, mesh=line, xlim=[-lims[i], lims[i]],
                      new=False, save=False)
    P1.plot_line_data(mod=mod7, mesh=line, xlim=[-lims[i], lims[i]],
                      new=False, save=False, sf=True)
    P1.plot_line_data(mod=mod8, mesh=line, xlim=[-lims[i], lims[i]],
                      new=False, save=False, sf=True)
    P1.plot_line_data(mod=mod9, mesh=line, xlim=[-lims[i], lims[i]], sf=True,
                      new=False, title='all_results_tot_and_sec_fields')
    plt.close('all')

for i, line in enumerate(lines):
    print('plotting mismatches: ', line)
    P1.plot_line_errors(mod=mod1, mesh=line, xlim=[-lims[i], lims[i]],
                        mod2=mod1, title=mod0 + '_and_' + mod1 + '_mismatch')
    P1.plot_line_errors(mod=mod8, mesh=line, xlim=[-lims[i], lims[i]], sf=True,
                        mod2=mod9,
                        title=mod7 + '_and_' + mod8 + '_sec_fields_mismatch')
    plt.close('all')

###############################################################################
# # #                     make default slice plots                        # # #
###############################################################################


P2 = Plot(mod=mod0, mesh=mesh1, approach='E_t', fig_size=[8, 12],
          r_dir='fd_test_results', s_dir='fd_test_plots/slice_plots')

for k in ['small', 'large']:
    P2.load_default_slice_data(interp_meshes=k)
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod1, sf=False, approach='E_s')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod2, sf=False, approach='E_s')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod3, mesh=mesh2, approach='Am_t')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod4, mesh=mesh2, approach='An_t')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod5, mesh=mesh2, sf=False, approach='Am_s')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod6, mesh=mesh2, sf=False, approach='Fm_s')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod7, mesh=mesh3, sf=True, approach='H_s')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod8, mesh=mesh3, sf=True, approach='H_s')
    P2.load_default_slice_data(interp_meshes=k,
                               mod=mod9, mesh=mesh3, sf=True, approach='An_s')

for i, slicE in enumerate(slices):
    print('plotting field data: ', slicE)
    P2.plot_slice_data(mod=mod0, mesh=slicE, title=mod0)
    P2.plot_slice_data(mod=mod1, mesh=slicE, title=mod1)
    P2.plot_slice_data(mod=mod2, mesh=slicE, title=mod2)
    P2.plot_slice_data(mod=mod3, mesh=slicE, title=mod3)
    P2.plot_slice_data(mod=mod4, mesh=slicE, title=mod4)
    P2.plot_slice_data(mod=mod5, mesh=slicE, title=mod5)
    P2.plot_slice_data(mod=mod6, mesh=slicE, title=mod6)
    P2.plot_slice_data(mod=mod7, mesh=slicE, sf=True,
                       title=mod7 + '_sec_fields')
    P2.plot_slice_data(mod=mod8, mesh=slicE, sf=True,
                       title=mod8 + '_sec_fields')
    P2.plot_slice_data(mod=mod9, mesh=slicE, sf=True,
                       title=mod9 + '_sec_fields')
    plt.close('all')

for i, slicE in enumerate(slices):
    print('plotting mismatches: ', slicE)
    P2.plot_slice_errors(mod=mod0, mod2=mod1, mesh=slicE,
                         title=mod0 + '_and_' + mod1 + '_mismatch')
    P2.plot_slice_errors(mod=mod8, mod2=mod9, mesh=slicE, sf=True,
                         title=mod8 + '_and_' + mod9 + '_sec_fields_mismatch')
    plt.close('all')
