# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from custEM.core import MOD
import dolfin as df
import shutil
from custEM.misc import block_print
from custEM.misc import enable_print
from custEM.misc import run_serial


#block_print()

"""
Generate meshes for the other tests from mpi script.
"""


mpi_cw = df.MPI.comm_world
mpi_size = df.MPI.size(mpi_cw)

if df.MPI.rank(df.MPI.comm_world) == 0:
    try:
        shutil.rmtree('meshes')
    except FileNotFoundError:
        pass
else:
    pass

df.MPI.barrier(mpi_cw)

# %%build meshes and convert them

run_serial('create_test_meshes_from_mpi.py')

# %% Convert to HDF 5 files to avoid interferences between time- and
#    frequency-domain tests

M = MOD('dummy', 'hed_halfspace_mesh', 'E_t', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='mesh_test_results',
        para_dir='meshes/para')
del M

M = MOD('dummy', 'line_dip_plate_mesh', 'E_t', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='mesh_test_results',
        para_dir='meshes/para')
del M

M = MOD('dummy', 'loop_brick_mesh', 'E_t', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='mesh_test_results',
        para_dir='meshes/para')
del M

M = MOD('dummy', 'sample_topo_mesh', 'E_t', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='mesh_test_results',
        para_dir='meshes/para')
del M


#enable_print()