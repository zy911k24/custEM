# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.misc.synthetic_definitions import sample_topo_func
from custEM.misc import block_print
from custEM.misc import enable_print
from mpi4py import MPI

"""
Create simple meshes for testing all time-domain approaches supported by
custEM and if the *pyGIMLi* and TetGen libraries are installed properly.
"""


def create_hed_mesh():

    M = BlankWorld(name='hed_halfspace_mesh',
                   m_dir='./meshes')

    M.build_surface(insert_line_tx=[mu.line_x(start=-0.5, stop=0.5, n_segs=5)])

    M.build_halfspace_mesh()

    M.call_tetgen(tet_param='-pq1.4aAQ')


def create_line_mesh():

    M = BlankWorld(name='line_dip_plate_mesh',
                   m_dir='./meshes')

    M.build_surface(insert_line_tx=[mu.line_x(
            start=-5e2, stop=5e2, n_segs=25)])

    M.build_layered_earth_mesh(2, [-500.])

    M.add_plate(500., 500., 200., [500.0, 100.0, -1000.], 45., 117.,
                cell_size=1e4)

    M.there_is_always_a_bigger_world(1e1, 1e1, 1e1)

    M.call_tetgen(tet_param='-pq1.4aAQ')


def create_loop_mesh():

    M = BlankWorld(name='loop_brick_mesh',
                   m_dir='./meshes')

    M.build_surface(insert_loop_tx=[mu.loop_r(start=[-5e2, -5e2],
                                              stop=[5e2, 5e2], n_segs=84)])

    M.build_halfspace_mesh()

    M.add_brick(start=[-1000., -300., -800.],
                stop=[-1500.0, 700.0, -400.], cell_size=1e5)

    M.call_tetgen(tet_param='-pq1.4aAQ')


def create_topo_mesh():

    M = BlankWorld(name='sample_topo_mesh',
                   m_dir='meshes',
                   topo=sample_topo_func)

    M.build_surface(insert_loop_tx=[mu.loop_r(start=[-500., -500.],
                                              stop=[500., 500.], n_segs=100)],
                    insert_lines=[mu.line_x(start=-4e2, stop=4e2, n_segs=8),
                                  mu.line_x(start=-5e3, stop=-6e2, n_segs=44),
                                  mu.line_x(start=6e2, stop=5e3, n_segs=44),
                                  mu.line_y(start=-4e2, stop=4e2, n_segs=8)])
    M.build_layered_earth_mesh(n_layers=3, layer_depths=[-500., -1200.])

    M.add_paths([mu.line_x(start=-5e3, stop=5e3, y=0., z=50., n_segs=200,
                            topo=M.topo, t_dir=M.t_dir)])

    M.add_plate(dx=500., dy=500., dz=100.,
                origin=[0.0, 0.0, -200.],
                dip=25.,
                dip_azimuth=17.,
                cell_size=1e3)

    M.there_is_always_a_bigger_world(1e1, 1e1, 1e1)

    M.call_tetgen(tet_param='-pq1.4aAQ')


if __name__ == "__main__":

    block_print()
    parent = MPI.Comm.Get_parent()
    create_hed_mesh()
    create_line_mesh()
    create_loop_mesh()
    create_topo_mesh()
    parent.Barrier()
    parent.Disconnect()
    enable_print()
