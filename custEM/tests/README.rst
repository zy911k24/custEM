################################################################################
# # # # #                             TESTS                            # # # # #
################################################################################

    Notice! The quality of the calculated fields of the test simulations
	is not good and representative, as rather minimalistic configurations 
	have been chosen for the test files.
	
	Notice! Even if only 1 process is used, the mpirun syntax is required.
	
################################################################################

    1. test_basics.py: 
    ==================

	--> mpirun -n 4 python test_basics.py  (1-4 procs recommended)

    This test checks if FEniCS and custEM are installed correctly and all paths
    are set correctly. Note: As long as "pyGIMLi" is not installed, only the
    *test_basics* test is supported.

################################################################################

    2. test_mesh_generation.py: 
    ===========================

	--> mpirun -n 1 python test_mesh_generation.py  (1 or more, does not matter)

    This test if TetGen works properly and generates the underlying meshes
	for the subsequent tests.

################################################################################

    3. test_fd_approaches.py:
    =========================

	--> mpirun -n 12 test_fd_approaches.py  (1-16 procs recommended)
	
    This test checks if all frequency-domain approaches and the "pyhed"
	library for primary-field computations work properly.

################################################################################

    4. test_td_approaches.py:
    =========================

	--> mpirun -n 8 test_td_approaches.py  (1-16 procs recommended)
	
    This test checks if all time-domain approaches work properly.
        
################################################################################

    5. test_topo_model.py:
    ======================

    --> mpirun -n 32 python test_topo_model.py	(12 - 64 procs recommended)

    This test checks if TetGen and all utility modules of custEM are working.
	It calculates a serious model with p2 in the second half of the test.
	This requires approximately 250 GB RAM with 12 procs.
    Using 32 parallel processes will reduce the computation time by
    ~30-50%, but increase the memory requirements by ~20%.
	The overall results or data interpolated on lines and slice can be
	viewed in Paraview or visualized with the *plot_tools*.
    
################################################################################