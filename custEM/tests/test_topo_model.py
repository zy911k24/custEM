# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import os
import shutil
from custEM.core import MOD
from custEM.misc import run_serial
from custEM.misc import block_print
from custEM.misc import enable_print


"""
Third test which checks if TetGen is available and if **meshgen_tools** are
working. The first run is with p1, the second one with p2. The latter requires
parallel computation and > 200 GB RAM since the problem has > 1M dofs.

Uncomment the **df.set_log_level(df.DEBUG)** command to see what FEniCS prints.
"""

ll = 20                           # custEM debug level for debugging

mpi_cw = df.MPI.comm_world
mpi_size = df.MPI.size(mpi_cw)

if df.MPI.rank(df.MPI.comm_world) == 0:
    try:
        shutil.rmtree('topo_test_results')
    except FileNotFoundError:
        pass
else:
    pass

df.MPI.barrier(mpi_cw)

# %% Create Mesh

if not os.path.isfile('meshes/_h5/sample_topo_mesh.h5'):
    run_serial('create_test_meshes_from_mpi.py')

# %% Computing Loop_R layered earth model with total field approach & p1 poly

M = MOD('p1', 'sample_topo_mesh', 'E_t', p=1, debug_level=ll,
        overwrite_results=True, overwrite_mesh=True, serial_ordering=True,
        m_dir='meshes', r_dir='topo_test_results')

M.MP.update_model_parameters(omega=1e2,
                             sigma_ground=[1e-2, 1e-1, 1e-3, 1e0])

M.FE.build_var_form(s_type='loop_r',
                    start=[-500., -500., 0.],
                    stop=[500., 500., 0.])

M.solve_main_problem()

# %% Initialize default interpoaltion meshes and interpolate p1 result on them

M = MOD('p1', 'sample_topo_mesh', 'E_t', p=1, debug_level=ll,
        m_dir='meshes', r_dir='topo_test_results',
        load_existing=True, overwrite_results=False)

M.IB.create_line_mesh('x', -5e3, 5e3, 100, z=0., force_create=True)
M.IB.create_line_mesh('x', -5e3, 5e3, 100, z=50., force_create=True)
M.IB.create_slice_mesh('z', 5e3, 100, z=0., force_create=True)
M.IB.create_slice_mesh('z', 5e3, 100, z=50., force_create=True)

M.IB.interpolate('x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_100_on_topo_line_x',
                 'E_t')
M.IB.interpolate('x0_-5000.0_x1_5000.0_y_0.0_z_50.0_n_100_on_topo_line_x',
                 'H_t')
M.IB.interpolate('dim_[5000.0, 5000.0]_x_0.0_y_0.0_z_0.0_n_[100, 100]_'
                 'on_topo_slice_z', 'E_t')
M.IB.interpolate('dim_[5000.0, 5000.0]_x_0.0_y_0.0_z_0.0_n_[100, 100]_'
                 'on_topo_slice_z', 'H_t')

if mpi_cw.Get_rank() == 0:
    print('\n')
    print('###########################################################')
    print('###########################################################')
    print('###########################################################')
    print('  Topo test performed successfully for p1 polynomials !!!')
    print('###########################################################')
    print('###########################################################')
    print('###########################################################')
else:
    pass

# %% Computing Loop_R layered earth model with total field approach & p2 poly

M = MOD('p2', 'sample_topo_mesh', 'E_t', p=2, debug_level=ll,
        overwrite_results=True, overwrite_mesh=True, serial_ordering=True,
        m_dir='meshes', r_dir='topo_test_results')

M.MP.update_model_parameters(omega=1e2,
                             sigma_ground=[1e-2, 1e-1, 1e-3, 1e0])

M.FE.build_var_form()   # use auto detection here
M.solve_main_problem()

# %% Interpolate p2 results
M.IB.interpolate('x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_100_on_topo_line_x',
                 'E_t')
M.IB.interpolate('x0_-5000.0_x1_5000.0_y_0.0_z_50.0_n_100_on_topo_line_x',
                 'H_t')
M.IB.interpolate('dim_[5000.0, 5000.0]_x_0.0_y_0.0_z_0.0_n_[100, 100]_'
                 'on_topo_slice_z', 'E_t')
M.IB.interpolate('dim_[5000.0, 5000.0]_x_0.0_y_0.0_z_0.0_n_[100, 100]_'
                 'on_topo_slice_z', 'H_t')

if mpi_cw.Get_rank() == 0:
    print('\n')
    print('###########################################################')
    print('###########################################################')
    print('###########################################################')
    print('  Topo test performed successfully for p2 polynomials !!!')
    print('###########################################################')
    print('###########################################################')
    print('###########################################################')
else:
    pass
