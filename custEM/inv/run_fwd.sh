# -*- coding: utf-8 -*-

fwd_name=$1
cfg_name=$2
n_cpu=$3
thread_id=$4

if [[ $thread_id == "0" ]]; then
	echo "================================================================================"
    echo "Starting custEM forward / Jacobian calculations via bash scripts"
fi

mpirun -n "$n_cpu" numactl python -u "$fwd_name" "$cfg_name" "$thread_id"
