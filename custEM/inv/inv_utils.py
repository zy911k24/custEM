# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import numpy as np
import pygimli as pg
import os
import sys
import json
import time
import custEM as cu
from mpi4py import MPI
from custEM.core import MOD
from subprocess import Popen
import matplotlib.pyplot as plt
from matplotlib.colors import SymLogNorm
from matplotlib.colors import LogNorm
from matplotlib.backends.backend_pdf import PdfPages
from saem import CSEMData


class MultiFWD(pg.Modelling):

    def __init__(self, mod, mesh, freqs=None, components=None, tx_ids=None,
                 saem_data=None, sig_bg=0.001, skip_domains=[0, 1],
                 max_procs=None, min_freqs=None, n_cores=144, p_fwd=1,
                 start_iter=0, inv_type='cells', n_layers=None,
                 print_interval=10, bind_procs=False, save_pvd=False,
                 set_zero_markers=True):

        """
        Initialize the model.
        """

        super().__init__()
        self.export_dir = 'results/E_t/' + mesh + '/' + mod
        self.inv_dir = 'inv_results/' + mod + '_' + mesh + '/'
        if not os.path.isdir(self.inv_dir):
            os.makedirs(self.inv_dir)

        if saem_data is not None:
            if freqs is not None or components is not None or \
               tx_ids is not None:
                print('Error! Specify either *saem_data* or '
                      '*components*, *freqs*, and *tx_ids*. Aborting  ...')
                raise SystemExit

            freqs = [freq for freq in saem_data["freqs"]]
            data_r, data_i = [], []
            error_r, error_i = [], []
            tx_ids, components = [], []

            for i, data in enumerate(saem_data["DATA"]):
                tx_ids.append(data["tx_ids"])
                components.append(data["cmp"])
                data_r = np.concatenate([data_r, data["dataR"].ravel()])
                data_i = np.concatenate([data_i, data["dataI"].ravel()])
                error_r = np.concatenate([error_r, data["errorR"].ravel()])
                error_i = np.concatenate([error_i, data["errorI"].ravel()])

            self.data_mask = np.isfinite(data_r + data_i + error_r + error_i)
            self.measured = np.hstack((data_r[self.data_mask],
                                       data_i[self.data_mask]))
            self.errors = np.abs(np.hstack((error_r[self.data_mask],
                                            error_i[self.data_mask])) /
                                 self.measured)

            alldata = np.hstack((data_r, data_i))
            np.save(self.inv_dir + 'datavec.npy', alldata)
            np.save(self.inv_dir + 'errorvec.npy',
                    np.abs(np.hstack((error_r, error_i)) / alldata))
            np.save(self.inv_dir + 'mask.npy', self.data_mask)
        else:
            self.data_mask = None

        for cmps in components:
            if any('B' in elem for elem in cmps):
                self.b_fields = True
            else:
                self.b_fields = False

            # support deprecated scripts with '_' field strings
            for elem in cmps:
                if '_' in elem:
                    elem = elem[0] + elem [2]

        self.mu_nt = np.pi * 400.
        self.inv_type = inv_type
        self.freqs = freqs
        self.n_freqs = len(freqs)
        self.components = components
        self.tx_ids = tx_ids
        self.sig_bg = sig_bg
        self.n_cores = n_cores
        self.n_layers = n_layers
        self.iteration = start_iter
        self.print_interval = print_interval

        self.config = dict()
        self.config['mod'] = mod
        self.config['mesh'] = mesh
        self.config['freqs'] = freqs
        self.config['components'] = components
        self.config['tx_ids'] = tx_ids
        self.config['p_fwd'] = p_fwd
        self.config['skip_domains'] = skip_domains
        self.config['save_pvd'] = save_pvd
        self.bind_procs = bind_procs

        self.init_mesh(mod, mesh, set_zero_markers)
        self.init_analyze()
        self.eval_parallel_layout(max_procs, min_freqs)

        if saem_data is not None:
            self.resort_ini_data()

        if start_iter > 0:
            self.sig_0 = np.load(self.inv_dir + 'sig_iter_' +
                                 str(self.iteration) + '.npy')
            print('  -  starting at iteration ' + str(start_iter) + '  - ')
        else:
            self.sig_0 = sig_bg
        self._jac = pg.Matrix()
        self.setJacobian(self._jac)

    def init_mesh(self, mod, mesh, set_zero_markers):

        # convert mesh and create export directories from root
        M = MOD(mod, mesh, 'E_t', overwrite_mesh=True, mesh_only=True,
                overwrite_results=True, export_domains=False)

        if self.inv_type == 'cells':
            h5mesh = pg.meshtools.readHDF5Mesh(
                'meshes/_h5/' + mesh + '.h5', group='domains',
                indices='cell_indices', pos='coordinates', cells='topology',
                marker='values', marker_default=0, dimension=3,
                verbose=True, useFenicsIndices=False)
            pgmesh = pg.Mesh(3)
            pgmesh.createMeshByMarker(h5mesh, 2)
            if set_zero_markers:
                pgmesh.setCellMarkers(pg.Vector(pgmesh.cellCount()))
        elif self.inv_type == 'domains':
            pgmesh = pg.load(M.MP.m_dir + '/mesh_create/' + mesh + '.bms')
        else:
            pgmesh = None

        if self.inv_type == 'layers':
            self.n_domains = self.n_layers
            self.n_res = self.n_layers
            self.mesh1d = pg.meshtools.createMesh1D(self.n_res)
            self.setMesh(self.mesh1d)
        if self.inv_type == 'domains':
            self.n_domains = M.FS.DOM.n_domains
            self.n_res = self.n_domains - len(self.config['skip_domains'])
            self.setMesh(pg.load(M.MP.m_dir + '/mesh_create/' + mesh + '.bms'))
        if self.inv_type == 'cells':
            self.n_domains = M.FS.DOM.n_domains
            self.n_res = self.n_domains - len(self.config['skip_domains'])
            self.setMesh(pgmesh)

        self.mesh().exportVTK(self.inv_dir + 'invmesh.vtk')
        self.rx = M.MP.rx
        self.tx = M.MP.tx
        self.n_rx = M.MP.n_rx
        self.n_tx = M.MP.n_tx

    def init_analyze(self):

        self.iters, self.chi2 = [], []
        if self.iteration > 0:
            tmp = np.loadtxt(self.inv_dir + 'chi2.dat', dtype=str)
            for j in range(self.iteration):
                self.iters.append(tmp[j, 0] + ' ' + tmp[j, 1] + ' ' +
                                  tmp[j, 2])
                self.chi2.append(float(tmp[j, 3]))

    def run_multi_fwds(self, sig, jacobian):

        """
        Forward response.
        """

        self.config['jacobian'] = jacobian
        self.config['iteration'] = self.iteration
        if type(sig) is list:
            self.sig = np.array(sig)
        else:
            self.sig = sig.array()

        for idx in self.config['skip_domains'][1:]:
            self.sig = np.insert(self.sig, idx-1, self.sig_bg)
        self.config['sig'] = self.sig.tolist()
        with open(self.export_dir + '_inv_config_' +
                  str(self.iteration) + '.json', "w") as outfile:
            json.dump(self.config, outfile, indent=0)

        custem_path = os.path.dirname(cu.__file__) + '/inv/'

        if self.bind_procs:
            to_call = ['bash', custem_path + 'run_fwd_bind.sh']
        else:
            to_call = ['bash', custem_path + 'run_fwd.sh']

        to_call.append(custem_path + 'run_fwd_from_config.py')
        to_call.append(self.export_dir + '_inv_config_' +
                       str(self.iteration) + '.json')
        to_call.append(str(self.n_procs))
        to_call.append(str(0))

        if self.bind_procs:
            start_proc = 0
            to_call.append(self.proc_ids[0])


        timed_out = False
        jobs = []
        returncodes = []

        t00 = time.time()
        for thread_id in range(self.n_threads):
            if thread_id != 0:
                if self.bind_procs:
                    to_call[-2] = str(thread_id)
                    start_proc += self.n_procs
                    to_call[-1] = self.proc_ids[thread_id]
                else:
                    to_call[-1] = str(thread_id)

            jobs.append(Popen(to_call, stdout=sys.stdout, stderr=sys.stdout))
            # wait a bit to avoid delays from simultaneous module imports
            time.sleep(1.)

        running = [jobs[job].poll() for job in range(self.n_threads)]
        while None in running:
            time.sleep(1.)
            running = [jobs[job].poll() for job in range(self.n_threads)]
            if (int(time.time() - t00) % self.print_interval) == 0:
                print('...  working  -->  time elapsed (s): ',
                      '{:.2f}'.format(time.time() - t00),
                      ' ...\n  - ', running, ' -  ')
            if 1 in running:
                thread_id = running.index(1)
                print('RESTARTING', running.index(1))
                print('Increasing n_procs by 2')
                to_call[-1] = str(thread_id)
                # raise n_procs fpr restart to speed up a bit
                to_call[-2] = str(self.n_procs + 2)
                jobs[thread_id] = Popen(to_call,
                                        stdout=sys.stdout, stderr=sys.stdout)

    def response(self, sig):

        """
        Import and resort fields.
        """

        self.run_multi_fwds(sig, None)
        self.import_fields()

        return(self.last_response)

    def createJacobian(self, sig):

        self.run_multi_fwds(sig, self.inv_type)
        self.iteration += 1
        self.import_fields(analyse=True)
        if self.inv_type == 'cells':
            self.import_local_jacobian()
        elif self.inv_type == 'domains':
            self.import_global_jacobian()

        self._jac.resize(*self.J.shape)
        for i, row in enumerate(self.J):
            self._jac.setRow(i, row)

    def import_fields(self, analyse=False):

        fields = np.array([], dtype=complex)
        for pi in range(self.n_rx):
            E = np.zeros((len(self.tx_ids[pi]), self.n_freqs,
                         len(self.rx[pi]), 3), dtype=complex)
            H = np.zeros((len(self.tx_ids[pi]), self.n_freqs,
                         len(self.rx[pi]), 3), dtype=complex)

            for ti in range(len(self.tx_ids[pi])):
                thread_id = 0
                for fi in range(self.n_freqs):
                    try:
                        E[:, fi, :, :] = np.load(
                            self.export_dir + '_thread_' + str(thread_id) +
                            '_interpolated/f_' + str(fi) +
                            '_E_t_all_tx_rx_path_' +
                            str(pi) + '.npy')[self.tx_ids[pi][ti], :, :]
                        H[:, fi, :, :] = np.load(
                            self.export_dir + '_thread_' + str(thread_id) +
                            '_interpolated/f_' + str(fi) +
                            '_H_t_all_tx_rx_path_' +
                            str(pi) + '.npy')[self.tx_ids[pi][ti], :, :]
                    except FileNotFoundError:
                        thread_id += 1
                        E[:, fi, :, :] = np.load(
                            self.export_dir + '_thread_' + str(thread_id) +
                            '_interpolated/f_' + str(fi) +
                            '_E_t_all_tx_rx_path_' +
                            str(pi) + '.npy')[self.tx_ids[pi][ti], :, :]
                        H[:, fi, :, :] = np.load(
                            self.export_dir + '_thread_' + str(thread_id) +
                            '_interpolated/f_' + str(fi) +
                            '_H_t_all_tx_rx_path_' +
                            str(pi) + '.npy')[self.tx_ids[pi][ti], :, :]

                if self.b_fields:
                    H *= self.mu_nt

                if analyse:
                    if pi == 0 and ti == 0:
                        self.pdf = PdfPages(self.inv_dir + 'response_iter_' +
                                            str(self.iteration) + '.pdf')
                    self.data_analysis(E, H, pi, ti)
                    if pi == self.n_rx - 1 and ti == len(self.tx_ids[pi]) - 1:
                        self.pdf.close()

                if 'Ex' in self.components[pi]:
                    fields = np.append(fields, E[ti, :, :, 0].ravel())
                if 'Ey' in self.components[pi]:
                    fields = np.append(fields, E[ti, :, :, 1].ravel())
                if 'Ez' in self.components[pi]:
                    fields = np.append(fields, E[ti, :, :, 2].ravel())
                if 'Hx' in self.components[pi] or 'Bx' in self.components[pi]:
                    fields = np.append(fields, H[ti, :, :, 0].ravel())
                if 'Hy' in self.components[pi] or 'By' in self.components[pi]:
                    fields = np.append(fields, H[ti, :, :, 1].ravel())
                if 'Hz' in self.components[pi] or 'Bz' in self.components[pi]:
                    fields = np.append(fields, H[ti, :, :, 2].ravel())


        if self.iteration == 0:
            np.save(self.inv_dir + 'resp_0.npy',
                    np.hstack((fields.real, fields.imag)))

        if self.data_mask is not None:
            fields =  fields[self.data_mask]
        self.last_response = np.hstack((fields.real, fields.imag))


    def import_global_jacobian(self):

        J_t = np.array([], dtype=complex).reshape(self.n_res, 0)

        for pi in range(self.n_rx):
            jacobi = np.zeros((self.n_res, len(self.tx_ids[pi]),
                                self.n_freqs, len(self.rx[pi]),
                                len(self.components[pi])), dtype=complex)

            thread_id = 0
            for fi in range(self.n_freqs):
                try:
                    jacobi[:, :, fi, :, :] = np.load(
                        self.export_dir + '_thread_' + str(thread_id) +
                        '_inv/f_' + str(fi) + '_J_rx_path_' + str(pi) +
                        '_iter_' + str(self.iteration - 1) + '.npy')
                except FileNotFoundError:
                    thread_id += 1
                    jacobi[:, :, fi, :, :] = np.load(
                        self.export_dir + '_thread_' + str(thread_id) +
                        '_inv/f_' + str(fi) + '_J_rx_path_' + str(pi) +
                        '_iter_' + str(self.iteration - 1) + '.npy')

            for ti in range(len(self.tx_ids[pi])):
                a = 0
                if 'Ex' in self.components[pi]:
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1
                if 'Ey' in self.components[pi]:
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1
                if 'Ez' in self.components[pi]:
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1
                if 'Hx' in self.components[pi] or 'Bx' in self.components[pi]:
                    if self.b_fields:
                        jacobi[:, ti, :, :, a] *= self.mu_nt
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1
                if 'Hy' in self.components[pi] or 'By' in self.components[pi]:
                    if self.b_fields:
                        jacobi[:, ti, :, :, a] *= self.mu_nt
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1
                if 'Hz' in self.components[pi] or 'Bz' in self.components[pi]:
                    if self.b_fields:
                        jacobi[:, ti, :, :, a] *= self.mu_nt
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1

        self.J = J_t.T
        if self.data_mask is not None:
            self.J =  self.J[self.data_mask]
        self.J = np.vstack((self.J.real, self.J.imag))

    def import_local_jacobian(self):

        J_locals = [[] for i in range(self.n_freqs)]

        t0 = time.time()
        thread_id = 0
        for fi in range(self.n_freqs):
            try:
                for ni in range(self.n_procs):
                    container = np.load(
                        self.export_dir + '_thread_' + str(thread_id) +
                        '_inv/f_' + str(fi) + '_J_proc_' + str(ni) + '.npz')
                    J_locals[fi].append([container[key] for
                                         key in container])
            except FileNotFoundError:
                thread_id += 1
                for ni in range(self.n_procs):
                    container = np.load(
                        self.export_dir + '_thread_' + str(thread_id) +
                        '_inv/f_' + str(fi) + '_J_proc_' + str(ni) + '.npz')
                    J_locals[fi].append([container[key] for
                                         key in container])

        print('import time', time.time() - t0)
        J_t = np.array([], dtype=complex).reshape(self.n_res, 0)

        for pi in range(self.n_rx):
            jacobi = np.zeros((self.n_res, len(self.tx_ids[pi]),
                               self.n_freqs, len(self.rx[pi]),
                               len(self.components[pi])), dtype=complex)

            for fi in range(self.n_freqs):
                for ni in range(self.n_procs):
                    if len(J_locals[fi][ni][-1]) > 0:
                        jacobi[J_locals[fi][ni][-1], :, fi, :, :] =\
                            J_locals[fi][ni][pi]

            for ti in range(len(self.tx_ids[pi])):
                a = 0
                if 'Ex' in self.components[pi]:
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1
                if 'Ey' in self.components[pi]:
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1
                if 'Ez' in self.components[pi]:
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1
                if 'Hx' in self.components[pi] or 'Bx' in self.components[pi]:
                    if self.b_fields:
                        jacobi[:, ti, :, :, a] *= self.mu_nt
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1
                if 'Hy' in self.components[pi] or 'By' in self.components[pi]:
                    if self.b_fields:
                        jacobi[:, ti, :, :, a] *= self.mu_nt
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1
                if 'Hz' in self.components[pi] or 'Bz' in self.components[pi]:
                    if self.b_fields:
                        jacobi[:, ti, :, :, a] *= self.mu_nt
                    J_t = np.hstack((
                        J_t, jacobi[:, ti, :, :, a].reshape(self.n_res, -1)))
                    a += 1

        self.J = J_t.T
        if self.data_mask is not None:
            self.J =  self.J[self.data_mask]
        self.J = np.vstack((self.J.real, self.J.imag))

    def eval_parallel_layout(self, max_procs, min_freqs):

        """
        Set up suited parallel layout depending on the workstation
        architecture. The argument *max_procs* has always priority. If not
        specified, the total available number of cores *n_cores* will be
        split into the maximum available number of cores for each job.
        If *min_freqs* is not adjusted, the problem will be split into
        *n_freqs* jobs.
        """

        if min_freqs is None:
            self.n_threads = self.n_freqs
            self.config['calc_freqs'] = [[fi] for fi in range(self.n_freqs)]
        else:
            if min_freqs == 2 and min_freqs < self.n_freqs:
                self.n_threads = int(self.n_freqs / 2) + 1
            elif min_freqs == 3 and min_freqs < self.n_freqs:
                self.n_threads = int(self.n_freqs / 3) + 1
            elif min_freqs == 4 and min_freqs < self.n_freqs:
                self.n_threads = int(self.n_freqs / 4) + 1
            elif min_freqs == self.n_freqs:
                self.n_threads = 1
            else:
                print('Error! Currently, *min_freqs* must be smaller than 4 '
                      'or equal to *n_freqs*. Aborting  ...')
                raise SystemExit
            counter = 0
            self.config['calc_freqs'] = []
            while counter < self.n_freqs:
                self.config['calc_freqs'].append(
                    [fi for fi in range(counter, counter + min_freqs)])
                counter += min_freqs

        if max_procs is None:
            self.n_procs = int(self.n_cores / self.n_threads)
        else:
            self.n_procs = max_procs

        if self.bind_procs:
            # if max_procs is None:
            #     self.n_procs = int((self.n_cores/4) /
            #                        (int(self.n_threads/4)+1))
            #     print(self.n_procs)
            #     ass
            # pass
            self.proc_ids = ['0-15,128-143',
                              '16-31,144-159',
                              '32-47,160-175',
                              '48-63,176-191',
                              '64-79,192-207',
                              '80-95,208-223',
                              '96-111,224-239',
                              '112-127,240-255']
            # self.proc_ids = ['0-30',
            #                  '32-62',
            #                  '64-94',
            #                  '96-126',
            #                  '128-158',
            #                  '160-190',
            #                  '192-222',
            #                  '224-254']

    def resort_ini_data(self):

        ini_data = self.measured
        if self.data_mask is not None:
            complex_data = np.zeros((len(self.data_mask)), dtype=complex)
            complex_data.real[self.data_mask] = ini_data[:int(len(ini_data)/2)]
            complex_data.imag[self.data_mask] = ini_data[int(len(ini_data)/2):]
        else:
            complex_data = ini_data[:int(len(ini_data)/2)] + \
                           1j * ini_data[int(len(ini_data)/2):]

        # if self.data_mask is not None:
        #     complex_data = complex_data[self.data_mask]

        self.measured_real = []
        self.measured_imag = []

        self.d_max = 10**np.ceil(np.log10(np.max(np.abs(ini_data))))
        self.d_min = 10**np.ceil(np.log10(np.min(np.abs(ini_data))) - 1)

        for pi in range(self.n_rx):
            if pi == 0:
                i1 = 0
            else:
                i1 = len(self.tx_ids[pi-1]) * self.n_freqs *\
                     len(self.rx[pi-1]) * len(self.components[pi-1])
            i2 = len(self.tx_ids[pi]) * self.n_freqs *\
                     len(self.rx[pi]) * len(self.components[pi]) + i1

            self.measured_real.append(complex_data.real[i1:i2].reshape(
                len(self.tx_ids[pi]), self.n_freqs,
                len(self.rx[pi]), len(self.components[pi])))
            self.measured_imag.append(complex_data.imag[i1:i2].reshape(
                len(self.tx_ids[pi]), self.n_freqs,
                len(self.rx[pi]), len(self.components[pi])))

    def analyze(self, iteration, inv):

        self.iters.append('iteration ' + str(self.iteration) + ' :')
        self.chi2.append(inv.chi2())
        np.savetxt(self.inv_dir + 'chi2.dat',
                   np.c_[self.iters, self.chi2], fmt='%14s %10s')

        np.save(self.inv_dir + 'sig_iter_' + str(self.iteration) + '.npy',
                inv.model)
        np.save(self.inv_dir + 'response_iter_' + str(self.iteration) +
                '.npy', self.last_response)

        inner = self.mesh()
        inner['res'] = 1./inv.model
        inner.exportVTK(self.inv_dir + 'latest_invmodel.vtk')

    def data_analysis(self, E, H, pi, ti):

        a = 0
        for cmp in self.components[pi]:
            if cmp == 'Ex':
                self.plot_response(E, pi, ti, 0, a, cmp)
                a += 1
            if cmp == 'Ey':
                self.plot_response(E, pi, ti, 1, a, cmp)
                a += 1
            if cmp == 'Ez':
                self.plot_response(E, pi, ti, 2, a, cmp)
                a += 1
            if cmp == 'Hx' or cmp == 'Bx':
                self.plot_response(H, pi, ti, 0, a, cmp)
                a += 1
            if cmp == 'Hy' or cmp == 'By':
                self.plot_response(H, pi, ti, 1, a, cmp)
                a += 1
            if cmp == 'Hz' or cmp == 'Bz':
                self.plot_response(H, pi, ti, 2, a, cmp)

    def plot_response(self, field, pi, ti, c, a, cmp):

        fig, ax = plt.subplots(3, 2, sharex=True, sharey=True, figsize=(8, 11))
        im = ax[0, 0].imshow(self.measured_real[pi][ti, :, :, a], cmap='PuOr',
                             norm=SymLogNorm(self.d_min, vmin=-self.d_max,
                                             vmax=self.d_max, base=10),
                             origin='lower')
        ax[0, 1].imshow(self.measured_imag[pi][ti, :, :, a], cmap='PuOr',
                        norm=SymLogNorm(self.d_min, vmin=-self.d_max,
                                        vmax=self.d_max, base=10),
                        origin='lower')
        ax[1, 0].imshow(field[ti, :, :, c].real, cmap='PuOr',
                        norm=SymLogNorm(self.d_min, vmin=-self.d_max,
                                        vmax=self.d_max, base=10),
                        origin='lower')
        ax[1, 1].imshow(field[ti, :, :, c].imag, cmap='PuOr',
                        norm=SymLogNorm(self.d_min, vmin=-self.d_max,
                                        vmax=self.d_max, base=10),
                        origin='lower')

        cbar = fig.colorbar(im, ax=ax[1, :2], orientation="horizontal")
        if 'E' in cmp:
            cbar.set_label('Amplitude (V/m)')
        if 'H' in cmp:
            cbar.set_label('Amplitude (A/m)')

        misfit_real = 100 * (self.measured_real[pi][ti, :, :, a] -
                             field[ti, :, :, c].real) /\
                      (np.abs(self.measured_real[pi][ti, :, :, a]) +
                       np.abs(field[ti, :, :, c].real))
        misfit_imag = 100 * (self.measured_imag[pi][ti, :, :, a] -
                             field[ti, :, :, c].imag) /\
                      (np.abs(self.measured_imag[pi][ti, :, :, a]) +
                       np.abs(field[ti, :, :, c].imag))
        er = ax[2, 0].imshow(misfit_real, cmap='coolwarm',
                             norm=SymLogNorm(1e0, vmin=-1e2,
                                             vmax=1e2, base=10),
                             origin='lower')
        ax[2, 1].imshow(misfit_imag, cmap='coolwarm',
                        norm=SymLogNorm(1e0, vmin=-1e2, vmax=1e2, base=10),
                        origin='lower')
        cbar2 = fig.colorbar(er, ax=ax[2, :2], orientation="horizontal")
        cbar2.set_label('Misfit (%)')

        for i in range(3):
            for j in range(2):
                ax[i, j].set_xlabel('Rx positions')
                ax[i, j].set_ylabel('f (Hz)')
                ax[i, j].set_yticks(np.arange(self.n_freqs))
                ax[i, j].set_yticklabels([str(f) for f in self.freqs])
                if j == 1:
                    ax[i, j].yaxis.set_label_position("right")
                    ax[i, j].yaxis.tick_right()

        ax[0, 0].set_title(r'$\Re$ (measured)')
        ax[0, 1].set_title(r'$\Im$ (measured)')
        ax[1, 0].set_title(r'$\Re$ (modeled)')
        ax[1, 1].set_title(r'$\Im$ (modeled)')
        ax[2, 0].set_title(r'misfit ($\%$)')
        ax[2, 1].set_title(r'misfit ($\%$)')

        plt.suptitle('Rx path : ' + str(pi) + ', Tx : ' + str(ti) + ', ' + cmp)
        self.pdf.savefig(fig)
        plt.close()

    def save_saemdata(self, save_dir, file_name, response):

        respR, respI = np.split(response, 2)
        respC = respR + respI*1j

        i = 0
        for ri in range(self.n_rx):

            respC_ri = respC[i, len(self.rx[ri])]

            cmp = [0, 0, 0]
            if 'Hx' in self.components[ri] or 'Bx' in self.components[ri]:
                cmp[0] = 1
            if 'Hy' in self.components[ri] or 'By' in self.components[ri]:
                cmp[1] = 1
            if 'Hz' in self.components[ri] or 'Bz' in self.components[ri]:
                cmp[2] = 1

            for ti in range(len(self.tx_ids[ri])):
                CSEM = CSEMData(txPos=self.tx[ti][:, :2].T,
                                rx=self.rx[ri][:, 0],
                                ry=self.rx[ri][:, 1],
                                rz=self.rx[ri][:, 2],
                                f=self.freqs,
                                cmp=cmp)

                CSEM.DATA = np.zeros(([3, CSEM.nF, CSEM.nRx]))
                tmp = np.reshape(respC_ri,
                    [sum(CSEM.cmp), len(self.tx_ids[ri]), CSEM.nF, CSEM.nRx])
                CSEM.DATA[np.nonzero(cmp)[0]] = tmp[:, ti, :, :]
                np.savez(save_dir + '/' + file_name + '_Rx_' + str(ri) +
                         '_Tx_' + str(ti) + '.npz', *data)

            i += len(self.rx[ri])

