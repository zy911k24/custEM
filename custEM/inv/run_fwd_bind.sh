# -*- coding: utf-8 -*-

fwd_name=$1
cfg_name=$2
n_cpu=$3
thread_id=$4
proc_ids=$5

if [[ $thread_id == "0" ]]; then
	echo "================================================================================"
    echo "Starting custEM forward / Jacobian calculations via bash scripts"
fi

echo $proc_ids
echo $n_cpu

mpirun -n "$n_cpu" numactl --physcpubind=$proc_ids python -u "$fwd_name" "$cfg_name" "$thread_id"
# --localalloc
# mpirun -n "$n_cpu" numactl python -u "$fwd_name" "$cfg_name" "$thread_id"
# could add --physcpubind=$start-$stop
# should add numactl --localalloc


# # check command output
# pysuccess=$?

# # exit script

# if [ $pysuccess -eq 0 ]; then
#     echo $pyscuccess
#     echo 'close mpi environment'
# else
#     echo $pyscuccess
#     echo 'encountered error'
#     exit 1
# fi
# The End
