# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""


from custEM.core import MOD
from custEM.misc import mpi_print as mpp
import sys
import json


config = json.load(open(sys.argv[1]))
thread_id = int(sys.argv[2])

# mpp('...  thread ' + str(thread_id) + ' calculating ' +
#     str(len(config['calc_freqs'][thread_id])) + ' frequencies  ...',
#     pre_dash=False)

M = MOD(config['mod'] + '_thread_' + str(thread_id),
        config['mesh'], 'E_t', p=config['p_fwd'], jacobian=config['jacobian'],
        debug_level=50, serial_ordering=True, export_domains=False,
        overwrite_results=True, overwrite_mesh=True)

M.MP.update_model_parameters(frequencies=config['freqs'],
                             sigma_ground=config['sig'])



if config['jacobian']:
    M.INV.update_inv_parameters(iteration=config['iteration'],
                                components=config['components'],
                                tx_ids=config['tx_ids'],
                                skip_domains=config['skip_domains'])

M.FE.build_var_form()

if config['jacobian']:
    M.solve_main_problem(
        convert=False,
        auto_interpolate=False,
        calculation_frequencies=config['calc_freqs'][thread_id])
else:
    M.solve_main_problem(
        auto_interpolate=True,
        export_pvd=config['save_pvd'],
        export_nedelec=config['save_pvd'],
        export_cg=False,
        calculation_frequencies=config['calc_freqs'][thread_id])

if config['save_pvd']:
    interp_meshes = []
    for ri, path in enumerate(M.MP.rx):
        M.IB.create_path_mesh(path, path_name=str(ri))
        interp_meshes.append(str(ri) + '_path')

    M.IB.interpolate(interp_meshes, ['E_t', 'H_t'],
                     freqs=config['calc_freqs'][thread_id])