
The four tutorial files contain the most important commands and parameter
options currently available in custEM. Users can comment or uncomment
all arguments and keyword arguments to explore the usage of custEM.
	
NOTICE
======

* Only the meshgen tutorials work as jupyter notebook. For running simulations,
  please use the provided Python script equivalent to the jupyter notebook. 
  Run simulations such as the **run_tutorial.py** python script by calling
  it from the command line with the *mpirun* syntax:

    --> mpirun -n 4 python run_tutorial.py

  Here, 4 parallel MPI processes are used.

Without modification, the provided "run" example requires about 7 GB of RAM
and can be conducted in serial or parallel (please remember that even in serial,
the "mpirun -n 1 python run_tutorial.py" syntax is required.

The *html* docs are also accessible from the main documentation of **custEM** 
The *run_tutorial* is independed from the **meshgen_tutorial**, but the
**plot_tutorial** depends on the **run_tutorial**. Therefore, for using the 
**plot_tutorial**, it is necessary to run the FE computations before.
   
################################################################################
